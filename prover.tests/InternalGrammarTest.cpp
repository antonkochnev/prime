#include "tinytests/tinytests.h"

#include "prover/InternalGrammar.h"

using namespace prime;

namespace
{
    class TestFormulaVisitor : public FormulaVisitor
    {
    public:
        virtual void handle(const NotFormula& fl) override
        {
            type = "~";
        }

        virtual void handle(const Variable& fl) override
        {
            type = "Var";
        }

        virtual void handle(const ConjunctionFormula& fl) override
        {
            type = "&";
        }

        virtual void handle(const DisjunctionFormula& fl) override
        {
            type = "|";
        }

        virtual void handle(const ImplicationFormula& fl) override
        {
            type = "=>";
        }

        virtual void handle(const PredicateFormula& fl) override
        {
            type = "()";
        }

    public:
        std::string type;
    };
}

TEST_SUITE_BEGIN(InternalGrammarTest)
    {
        add("TestConvert", &InternalGrammarTest::TestConvert);
    }

    void checkFromula(const std::string& str, const std::string& type, const std::string& expectedRestored)
    {
        try
        {
            InternalGrammar grammar;
            const auto fl = grammar.formula(str.c_str());
            assertNotNull(fl);
            TestFormulaVisitor visitor;
            fl->visit(visitor);
            assertEquals(type, visitor.type);
            const std::string restored = grammar.print(*fl);
            assertEquals(expectedRestored, restored);
        }
        catch (const std::exception&)
        {
            std::cout << "FAILED: check " << str << std::endl;
            throw;
        }
    }

    void checkSequent(const std::string& str, const std::string& expectedRestored)
    {
        InternalGrammar grammar;
        const auto sequent = grammar.sequent(str.c_str());
        const std::string restored = grammar.print(sequent);
        assertEquals(expectedRestored, restored);
    }

    void TestConvert()
    {
        checkFromula("A", "Var", "A");
        checkFromula("~A", "~", "~A");
        checkFromula("( A)", "Var", "A");
        checkFromula("(~A)", "~", "~A");
        checkFromula("(A&B)", "&", "A&B");

        checkSequent("|-A", "|-A");
        checkSequent("B|-A", "B|-A");
        checkSequent("A&B|-A", "A&B|-A");
    }
TEST_SUITE_END
