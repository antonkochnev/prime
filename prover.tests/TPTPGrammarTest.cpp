#include "tinytests/tinytests.h"

#include "prover/TPTPGrammar.h"

using namespace prime;

TEST_SUITE_BEGIN(TPTPGrammarTest)
{
    add("TestConvert", &TPTPGrammarTest::TestConvert);
}

void TestConvert()
{
    TPTPGrammar grammar;

    {
        const auto input = grammar.input("include('Axioms/SET001-0.ax').");
    }

    {
        const auto input = grammar.input("cnf(b_equals_bb,hypothesis, (equal_sets(b,bb))).");
    }

    {
        const auto input = grammar.input("cnf(membership_in_subsets, axiom,\n    (~member(Element, Subset)\n   | ~subset(Subset, Superset)\n| member(Element, Superset))).");

    }
}
TEST_SUITE_END
