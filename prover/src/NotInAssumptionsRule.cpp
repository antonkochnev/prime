#include "prover/NotInAssumptionsRule.h"

namespace prime
{
    NotInAssumptionsRule::NotInAssumptionsRule(std::size_t inx)
        : inx_(inx)
    {}

    std::string NotInAssumptionsRule::description() const
    {
        return "";
    }

    Sequent NotInAssumptionsRule::apply(const Sequent& src)
    {
        Sequent res = src;
        return res;
    }
}
