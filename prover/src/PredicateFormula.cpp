#include "prover/PredicateFormula.h"
#include "prover/FormulaVisitor.h"

namespace prime
{
    PredicateFormula::PredicateFormula(const std::string& functor, const std::vector<FormulaPtr>& args)
        : functor_(functor), args_(args)
    {}

    FormulaPtr PredicateFormula::clone() const
    {
        return std::make_shared<PredicateFormula>(functor_, args_);
    }

    void PredicateFormula::visit(FormulaVisitor& visitor) const
    {
        visitor.handle(*this);
    }

    const std::vector<FormulaPtr>& PredicateFormula::args() const
    {
        return args_;
    }

    const std::string& PredicateFormula::functor() const
    {
        return functor_;
    }
}
