#include "prover/FirstProverStrategy.h"

#include "prover/FormulaComparator.h"

namespace prime
{
    void FirstProverStrategy::apply(ProofNode& node)
    {
    }

    bool FirstProverStrategy::isAxiom(const Sequent& sequent)
    {
        FormulaComparator comparator;

        for (const auto& fl1 : sequent.assumptions)
        {
            for (const auto& fl2 : sequent.conjecture)
            {
                if (comparator.equals(*fl1, *fl2))
                {
                    return true;
                }
            }
        }

        return false;
    }
}
