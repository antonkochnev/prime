#include "prover/FormulaComparator.h"
#include "prover/InternalGrammar.h"

namespace prime
{
    bool FormulaComparator::equals(const Formula& arg1, const Formula& arg2)
    {
        return (InternalGrammar::print(arg1) == InternalGrammar::print(arg2));
    }
}
