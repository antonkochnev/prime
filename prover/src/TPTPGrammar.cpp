#include "prover/TPTPGrammar.h"

#include <iostream>
#include <fstream>

#include "grammar/NonterminalUtils.h"
#include "grammar/PEGEarleyParser.h"
#include "grammar/ParsingExpressionGrammar.h"

#define CC(name) PEG::CharacterClass name(grammar_, #name);
#define TOK(name) PEG::Token name(grammar_, #name);
#define DEF(name) PEG::ProductionRule name(grammar_, #name);

namespace prime
{
    TPTPGrammar::TPTPGrammar()
    :
        grammar_("TPTPGrammar")
    {
        CC(printable_char);
        CC(dollar);
        CC(alpha_numeric);
        CC(upper_alpha);
        CC(lower_alpha);
        CC(numeric);
        CC(non_zero_numeric);
        CC(zero_numeric);
        CC(slash);
        CC(exponent);
        CC(dot);
        CC(sign);
        CC(sq_char);
        CC(single_quote);
        CC(do_char);
        CC(double_quote);
        CC(percentage_sign);
        TOK(unsigned_exp_integer);
        TOK(signed_exp_integer);
        TOK(exp_integer);
        TOK(dot_decimal);
        TOK(decimal_fraction);
        TOK(decimal_exponent);
        TOK(positive_decimal);
        TOK(decimal);
        TOK(unsigned_integer);
        TOK(signed_integer);
        TOK(integer);
        TOK(unsigned_rational);
        TOK(signed_rational);
        TOK(rational);
        TOK(unsigned_real);
        TOK(signed_real);
        TOK(real);
        TOK(less_sign);
        TOK(arrow);
        TOK(plus);
        TOK(star);
        TOK(vline);
        TOK(lower_word);
        TOK(upper_word);
        TOK(dollar_dollar_word);
        TOK(dollar_word);
        TOK(distinct_object);
        TOK(single_quoted);
        CC(not_star_slash);
        CC(comment_block);
        TOK(comment_line);
        TOK(comment);
        DEF(null);
        DEF(file_name);
        DEF(number);
        DEF(atomic_system_word);
        DEF(atomic_defined_word);
        DEF(atomic_word);
        DEF(name);
        DEF(general_terms);
        DEF(general_list);
        DEF(formula_data);
        DEF(general_data);
        DEF(general_function);
        DEF(general_term);
        DEF(name_list);
        DEF(formula_selection);
        DEF(include);
        DEF(principal_symbol);
        DEF(new_symbol_list);
        DEF(new_symbol_record);
        DEF(refutation);
        DEF(assumptions_record);
        DEF(inference_info);
        DEF(status_value);
        DEF(inference_status);
        DEF(inference_item);
        DEF(iquote_item);
        DEF(description_item);
        DEF(formula_item);
        DEF(info_item);
        DEF(info_items);
        DEF(useful_info);
        DEF(optional_info);
        DEF(creator_name);
        DEF(creator_source);
        DEF(theory_name);
        DEF(theory);
        DEF(file_info);
        DEF(file_source);
        DEF(external_source);
        DEF(intro_type);
        DEF(internal_source);
        DEF(parent_details);
        DEF(parent_info);
        DEF(parent_list);
        DEF(inference_parents);
        DEF(inference_rule);
        DEF(inference_record);
        DEF(dag_source);
        DEF(sources);
        DEF(source);
        DEF(variable);
        DEF(defined_term);
        DEF(defined_functor);
        DEF(defined_constant);
        DEF(system_functor);
        DEF(system_constant);
        DEF(functor);
        DEF(constant);
        DEF(infix_inequality);
        DEF(infix_equality);
        DEF(defined_infix_pred);
        DEF(defined_predicate);
        DEF(defined_proposition);
        DEF(untyped_atom);
        DEF(atom);
        DEF(system_type);
        DEF(defined_type);
        DEF(type_functor);
        DEF(type_constant);
        DEF(assignment);
        DEF(gentzen_arrow);
        DEF(unary_connective);
        DEF(assoc_connective);
        DEF(binary_connective);
        DEF(fof_quantifier);
        DEF(subtype_sign);
        DEF(th1_unary_connective);
        DEF(thf_unary_connective);
        DEF(thf_pair_connective);
        DEF(th0_quantifier);
        DEF(th1_quantifier);
        DEF(thf_quantifier);
        DEF(literal);
        DEF(disjunction);
        DEF(cnf_formula);
        DEF(fof_formula_tuple_list);
        DEF(fof_formula_tuple);
        DEF(fof_sequent);
        DEF(tff_tuple_term);
        DEF(tff_let_term);
        DEF(tff_conditional_term);
        DEF(fof_function_term);
        DEF(fof_term);
        DEF(fof_arguments);
        DEF(fof_system_term);
        DEF(fof_defined_plain_term);
        DEF(fof_defined_atomic_term);
        DEF(fof_defined_term);
        DEF(fof_plain_term);
        DEF(fof_system_atomic_formula);
        DEF(fof_defined_infix_formula);
        DEF(fof_defined_plain_formula);
        DEF(fof_defined_atomic_formula);
        DEF(fof_plain_atomic_formula);
        DEF(fof_atomic_formula);
        DEF(fof_infix_unary);
        DEF(fof_unary_formula);
        DEF(fof_variable_list);
        DEF(fof_quantified_formula);
        DEF(fof_unitary_formula);
        DEF(fof_and_formula);
        DEF(fof_or_formula);
        DEF(fof_binary_assoc);
        DEF(fof_binary_nonassoc);
        DEF(fof_binary_formula);
        DEF(fof_logic_formula);
        DEF(fof_formula);
        DEF(tcf_quantified_formula);
        DEF(tcf_logic_formula);
        DEF(tcf_formula);
        DEF(tff_xprod_type);
        DEF(tff_mapping_type);
        DEF(tff_type_arguments);
        DEF(tff_atomic_type);
        DEF(tff_unitary_type);
        DEF(tff_monotype);
        DEF(tf1_quantified_type);
        DEF(tff_top_level_type);
        DEF(tff_subtype);
        DEF(tff_typed_atom);
        DEF(tff_formula_tuple_list);
        DEF(tff_formula_tuple);
        DEF(tff_sequent);
        DEF(tff_let_formula_binding);
        DEF(tff_let_formula_defn);
        DEF(tff_let_formula_list);
        DEF(tff_let_formula_defns);
        DEF(tff_let_term_binding);
        DEF(tff_let_term_defn);
        DEF(tff_let_term_list);
        DEF(tff_let_term_defns);
        DEF(tff_let);
        DEF(tff_conditional);
        DEF(tff_atomic_formula);
        DEF(tff_unary_formula);
        DEF(tff_typed_variable);
        DEF(tff_variable);
        DEF(tff_variable_list);
        DEF(tff_quantified_formula);
        DEF(tff_unitary_formula);
        DEF(tff_and_formula);
        DEF(tff_or_formula);
        DEF(tff_binary_assoc);
        DEF(tff_binary_nonassoc);
        DEF(tff_binary_formula);
        DEF(tff_logic_formula);
        DEF(tff_formula);
        DEF(tfx_logic_formula);
        DEF(tfx_formula);
        DEF(logic_defn_value);
        DEF(logic_defn_RHS);
        DEF(logic_defn_LHS);
        DEF(logic_defn_rule);
        DEF(thf_formula_list);
        DEF(thf_tuple);
        DEF(thf_sequent);
        DEF(thf_union_type);
        DEF(thf_xprod_type);
        DEF(thf_mapping_type);
        DEF(thf_binary_type);
        DEF(thf_apply_type);
        DEF(thf_unitary_type);
        DEF(thf_top_level_type);
        DEF(thf_type_formula);
        DEF(thf_subtype);
        DEF(thf_typeable_formula);
        DEF(thf_arguments);
        DEF(thf_let_defn_LHS);
        DEF(thf_let_plain_defn);
        DEF(thf_let_quantified_defn);
        DEF(thf_let_defn);
        DEF(thf_let_defn_list);
        DEF(thf_let_defns);
        DEF(thf_let);
        DEF(thf_conditional);
        DEF(thf_conn_term);
        DEF(thf_function);
        DEF(thf_atom);
        DEF(thf_unary_formula);
        DEF(thf_typed_variable);
        DEF(thf_variable);
        DEF(thf_variable_list);
        DEF(thf_quantification);
        DEF(thf_quantified_formula);
        DEF(thf_unitary_formula);
        DEF(thf_apply_formula);
        DEF(thf_and_formula);
        DEF(thf_or_formula);
        DEF(thf_binary_tuple);
        DEF(thf_binary_pair);
        DEF(thf_binary_formula);
        DEF(thf_logic_formula);
        DEF(thf_formula);
        DEF(formula_role);
        DEF(annotations);
        DEF(cnf_annotated);
        DEF(fof_annotated);
        DEF(tcf_annotated);
        DEF(tff_annotated);
        DEF(tfx_annotated);
        DEF(thf_annotated);
        DEF(tpi_formula);
        DEF(tpi_annotated);
        DEF(annotated_formula);
        DEF(TPTP_input);
        DEF(TPTP_file);
        printable_char << " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
        dollar << "$";
        upper_alpha << "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        lower_alpha << "abcdefghijklmnopqrstuvwxyz";
        numeric << "0123456789";
        alpha_numeric << lower_alpha | upper_alpha | numeric | '_';
        non_zero_numeric << "123456789";
        zero_numeric << "0";
        slash << "/";
        exponent << "Ee";
        dot << ".";
        sign << "+-";
        sq_char << " !\"#$%&()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_`abcdefghijklmnopqrstuvwxyz{|}~";
        single_quote << "'";
        do_char << " !#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_`abcdefghijklmnopqrstuvwxyz{|}~";
        double_quote << "\"";
        percentage_sign << "%";
        unsigned_exp_integer << *numeric;
        signed_exp_integer << sign & unsigned_exp_integer;
        exp_integer << signed_exp_integer | unsigned_exp_integer;
        dot_decimal << dot & *numeric;
        decimal_fraction << decimal & dot_decimal;
        decimal_exponent << decimal & exponent & exp_integer;
        decimal_exponent << decimal_fraction & exponent & exp_integer;
        positive_decimal << non_zero_numeric;
        positive_decimal << positive_decimal & *numeric;
        decimal << zero_numeric | positive_decimal;
        unsigned_integer << decimal;
        signed_integer << sign & unsigned_integer;
        integer << signed_integer | unsigned_integer;
        unsigned_rational << decimal & slash & positive_decimal;
        signed_rational << sign & unsigned_rational;
        rational << signed_rational | unsigned_rational;
        unsigned_real << decimal_fraction | decimal_exponent;
        signed_real << sign & unsigned_real;
        real << signed_real | unsigned_real;
        less_sign << "<";
        arrow << ">";
        plus << "+";
        star << "*";
        vline << "|";
        lower_word << lower_alpha;
        lower_word << lower_word & *alpha_numeric;
        upper_word << upper_alpha;
        upper_word << upper_word & *alpha_numeric;
        dollar_dollar_word << dollar & dollar & lower_word;
        dollar_word << dollar & lower_word;
        distinct_object << double_quote & double_quote;
        distinct_object << double_quote & *do_char & double_quote;
        single_quoted << single_quote & *sq_char & single_quote;
        file_name << single_quoted;
        number << integer | rational | real;
        atomic_system_word << dollar_dollar_word;
        atomic_defined_word << dollar_word;
        atomic_word << lower_word | single_quoted;
        name << atomic_word | integer;
        general_terms << general_term | general_term & "," & general_terms;
        general_list << "[" & "]";
        general_list << "[" & general_terms & "]";
        formula_data << "$thf(" & thf_formula & ")";
        formula_data << "$tff(" & tff_formula & ")";
        formula_data << "$fof(" & fof_formula & ")";
        formula_data << "$cnf(" & cnf_formula & ")";
        formula_data << "$fot(" & fof_term & ")";
        general_data << "bind(" & variable & "," & formula_data & ")";
        general_function << atomic_word & "(" & general_terms & ")";
        general_data << atomic_word | general_function | variable | number | distinct_object | formula_data;
        general_term << general_data;
        general_term << general_data & ":" & general_term;
        general_term << general_list;
        name_list << name;
        name_list << name & "," & name_list;
        formula_selection << "," & "[" & name_list & "]";
        include << "include(" & file_name & formula_selection & ").";
        include << "include(" & file_name & ").";
        principal_symbol << functor | variable;
        new_symbol_list << principal_symbol;
        new_symbol_list << principal_symbol & "," & new_symbol_list;
        new_symbol_record << "new_symbols(" & atomic_word & "," & "[" & new_symbol_list & "]" & ")";
        refutation << "refutation(" & file_source & ")";
        assumptions_record << "assumptions([" & name_list & "])";
        inference_info << inference_rule & "(" & atomic_word & "," & general_list & ")";
        status_value << "suc" | "unp" | "sap" | "esa" | "sat" | "fsa" | "thm" | "eqv" | "tac" | "wec" |
            "eth" | "tau" | "wtc" | "wth" | "cax" | "sca" | "tca" | "wca" | "cup" | "csp" | "ecs" | "csa" |
            "cth" | "ceq" | "unc" | "wcc" | "ect" | "fun" | "uns" | "wuc" | "wct" | "scc" | "uca" | "noc";
        inference_status << "status(" & status_value & ")";
        inference_status << "inference_info";
        inference_item << inference_status | assumptions_record | new_symbol_record | refutation;
        iquote_item << "iquote(" & atomic_word & ")";
        description_item << "description(" & atomic_word & ")";
        formula_item << description_item | iquote_item;
        info_item << formula_item | inference_item | general_function;
        info_items << info_item | info_item & "," & info_items;
        useful_info << "[" & "]";
        useful_info << "[" & info_items & "]";
        useful_info << general_list;
        optional_info << "," & useful_info;
        optional_info << null;
        creator_name << atomic_word;
        creator_source << "creator(" & creator_name & optional_info & ")";
        theory_name << "equality" | "ac";
        theory << "theory(" & theory_name & optional_info & ")";
        file_info << "," & name;
        file_info << null;
        file_source << "file(" & file_name & file_info & ")";
        external_source << file_source | theory | creator_source;
        intro_type << "definition" | "axiom_of_choice" | "tautology" | "assumption";
        internal_source << "introduced(" & intro_type & optional_info & ")";
        parent_details << ":" & general_list;
        parent_details << null;
        parent_info << source & parent_details;
        parent_list << parent_info;
        parent_list << parent_info & "," & parent_list;
        inference_parents << "[" & "]";
        inference_parents << "[" & parent_list & "]";
        inference_rule << atomic_word;
        inference_record << "inference(" & inference_rule & "," & useful_info & "," & inference_parents & ")";
        dag_source << name | inference_record;
        sources << source;
        sources << source & "," & sources;
        source << dag_source | internal_source | external_source | "unknown";
        source << "[" & sources & "]";
        source << general_term;
        variable << upper_word;
        defined_term << number | distinct_object;
        defined_functor << "$uminus" | "$sum" | "$difference" | "$product" | "$quotient" | "$quotient_e" | "$quotient_t" | "$quotient_f" |
            "$remainder_e" | "$remainder_t" | "$remainder_f" | "$floor" | "$ceiling" | "$truncate" | "$round" | "$to_int" | "$to_rat" | "$to_real";
        defined_functor << atomic_defined_word;
        defined_constant << defined_functor;
        system_functor << atomic_system_word;
        system_constant << system_functor;
        functor << atomic_word;
        constant << functor;
        infix_inequality << "!=";
        infix_equality << "=";
        defined_infix_pred << infix_equality | assignment;
        defined_predicate << "$distinct" | "$less" | "$lesseq" | "$greater" | "$greatereq" | "$is_int" | "$is_rat" | "$box_P" | "$box_i" | "$box_int" |
            "$box" | "$dia_P" | "$dia_i" | "$dia_int" | "$dia";
        defined_predicate << atomic_defined_word;
        defined_proposition << "$true" | "$false";
        defined_proposition << atomic_defined_word;
        untyped_atom << constant | system_constant;
        atom << untyped_atom | defined_constant;
        system_type << atomic_system_word;
        defined_type << "$oType" | "$o" | "$iType" | "$i" | "$tType" | "$real" | "$rat" | "$int";
        defined_type << atomic_defined_word;
        type_functor << atomic_word;
        type_constant << type_functor;
        assignment << ":=";
        gentzen_arrow << "-->";
        unary_connective << "~";
        assoc_connective << vline | "&";
        binary_connective << "<=>" | "=>" | "<=" | "<~>";
        binary_connective << "~" & vline;
        binary_connective << "~&";
        fof_quantifier << "!" | "?";
        subtype_sign << "<<";
        th1_unary_connective << "!!" | "??" | "@@+" | "@@-" | "@=";
        thf_unary_connective << unary_connective | th1_unary_connective;
        thf_pair_connective << infix_equality | infix_inequality | binary_connective | assignment;
        th0_quantifier << "^"| "@+" | "@-";
        th1_quantifier << "!>" | "?*";
        thf_quantifier << fof_quantifier | th0_quantifier | th1_quantifier;
        literal << fof_atomic_formula;
        literal << "~" & fof_atomic_formula;
        literal << fof_infix_unary;
        disjunction << literal;
        disjunction << disjunction & vline & literal;
        cnf_formula << disjunction;
        cnf_formula << "(" & disjunction & ")";
        fof_formula_tuple_list << fof_logic_formula;
        fof_formula_tuple_list << fof_logic_formula & "," & fof_formula_tuple_list;
        fof_formula_tuple << "[]";
        fof_formula_tuple << "[" & fof_formula_tuple_list & "]";
        fof_sequent << fof_formula_tuple & gentzen_arrow & fof_formula_tuple;
        fof_sequent << "(" & fof_sequent & ")";
        tff_tuple_term << "{} | {" & fof_arguments & "}";
        tff_let_term << "$let_ft(" & tff_let_formula_defns & "," & fof_term & ")";
        tff_let_term << "$let_tt(" & tff_let_term_defns & ", " & fof_term & ")";
        tff_conditional_term << "$ite_t(" & tff_logic_formula & "," & fof_term & "," & fof_term & ")";
        fof_function_term << fof_plain_term | fof_defined_term | fof_system_term;
        fof_term << fof_function_term | variable | tff_conditional_term | tff_let_term | tff_tuple_term;
        fof_arguments << fof_term;
        fof_arguments << fof_term & "," & fof_arguments;
        fof_system_term << system_constant;
        fof_system_term << system_functor & "(" & fof_arguments & ")";
        fof_defined_plain_term << defined_constant;
        fof_defined_plain_term << defined_functor & "(" & fof_arguments & ")";
        fof_defined_atomic_term << fof_defined_plain_term;
        fof_defined_term << defined_term | fof_defined_atomic_term;
        fof_plain_term << constant;
        fof_plain_term << functor & "(" & fof_arguments & ")";
        fof_system_atomic_formula << fof_system_term;
        fof_defined_infix_formula << fof_term & defined_infix_pred & fof_term;
        fof_defined_plain_formula << defined_proposition;
        fof_defined_plain_formula << defined_predicate & "(" & fof_arguments & ")";
        fof_defined_plain_formula << fof_defined_plain_term;
        fof_defined_atomic_formula << fof_defined_plain_formula | fof_defined_infix_formula;
        fof_plain_atomic_formula << defined_proposition;
        fof_plain_atomic_formula << defined_predicate & "(" & fof_arguments & ")";
        fof_plain_atomic_formula << fof_plain_term;
        fof_atomic_formula << fof_plain_atomic_formula | fof_defined_atomic_formula | fof_system_atomic_formula;
        fof_infix_unary << fof_term & infix_inequality & fof_term;
        fof_unary_formula << unary_connective & fof_unitary_formula;
        fof_unary_formula << fof_infix_unary;
        fof_variable_list << variable;
        fof_variable_list << variable & "," & fof_variable_list;
        fof_quantified_formula << fof_quantifier & "[" & fof_variable_list & "] :" & fof_unitary_formula;
        fof_unitary_formula << fof_quantified_formula | fof_unary_formula | fof_atomic_formula;
        fof_unitary_formula << "(" & fof_logic_formula & ")";
        fof_and_formula << fof_unitary_formula & "&" & fof_unitary_formula;
        fof_and_formula << fof_and_formula & "&" & fof_unitary_formula;
        fof_or_formula << fof_unitary_formula & vline & fof_unitary_formula;
        fof_or_formula << fof_or_formula & vline & fof_unitary_formula;
        fof_binary_assoc << fof_or_formula | fof_and_formula;
        fof_binary_nonassoc << fof_unitary_formula & binary_connective & fof_unitary_formula;
        fof_binary_formula << fof_binary_nonassoc | fof_binary_assoc;
        fof_logic_formula << fof_binary_formula | fof_unitary_formula;
        fof_formula << fof_logic_formula | fof_sequent;
        tcf_quantified_formula << "!" & "[" & tff_variable_list & "] :" & cnf_formula;
        tcf_logic_formula << tcf_quantified_formula | cnf_formula;
        tcf_formula << tcf_logic_formula | tff_typed_atom;
        tff_xprod_type << tff_unitary_type & star & tff_atomic_type;
        tff_xprod_type << tff_xprod_type & star & tff_atomic_type;
        tff_mapping_type << tff_unitary_type & arrow & tff_atomic_type;
        tff_type_arguments << tff_atomic_type;
        tff_type_arguments << tff_atomic_type & "," & tff_type_arguments;
        tff_atomic_type << type_constant | defined_type;
        tff_atomic_type << type_functor & "(" & tff_type_arguments & ")";
        tff_atomic_type << variable;
        tff_unitary_type << tff_atomic_type;
        tff_unitary_type << "(" & tff_xprod_type & ")";
        tff_monotype << tff_atomic_type;
        tff_monotype << "(" & tff_mapping_type & ")";
        tf1_quantified_type << "!>" & "[" & tff_variable_list & "] :" & tff_monotype;
        tff_top_level_type << tff_atomic_type | tff_mapping_type | tf1_quantified_type;
        tff_top_level_type << "(" & tff_top_level_type & ")";
        tff_subtype << untyped_atom & subtype_sign & atom;
        tff_typed_atom << untyped_atom & ":" & tff_top_level_type;
        tff_typed_atom << "(" & tff_typed_atom & ")";
        tff_formula_tuple_list << tff_logic_formula;
        tff_formula_tuple_list << tff_logic_formula & "," & tff_formula_tuple_list;
        tff_formula_tuple << "[]";
        tff_formula_tuple << "[" & tff_formula_tuple_list & "]";
        tff_sequent << tff_formula_tuple & gentzen_arrow & tff_formula_tuple;
        tff_sequent << "(" & tff_sequent & ")";
        tff_let_formula_binding << fof_plain_atomic_formula & "<=>" & tff_unitary_formula;
        tff_let_formula_binding << "(" & tff_let_formula_binding & ")";
        tff_let_formula_defn << "!" & "[" & tff_variable_list & "]" & ":" & tff_let_formula_defn;
        tff_let_formula_defn << tff_let_formula_binding;
        tff_let_formula_list << tff_let_formula_defn;
        tff_let_formula_list << tff_let_formula_defn & "," & tff_let_formula_list;
        tff_let_formula_defns << tff_let_formula_defn;
        tff_let_formula_defns << "[" & tff_let_formula_list & "]";
        tff_let_term_binding << fof_plain_term & "=" & fof_term;
        tff_let_term_binding << "(" & tff_let_term_binding & ")";
        tff_let_term_defn << "!" & "[" & tff_variable_list & "] :" & tff_let_term_defn;
        tff_let_term_defn << tff_let_term_binding;
        tff_let_term_list << tff_let_term_defn;
        tff_let_term_list << tff_let_term_defn & "," & tff_let_term_list;
        tff_let_term_defns << tff_let_term_defn;
        tff_let_term_defns << "[" & tff_let_term_list & "]";
        tff_let << "$let_tf(" & tff_let_term_defns & "," & tff_formula & ")";
        tff_let << "$let_ff(" & tff_let_formula_defns & ", " & tff_formula & ")";
        tff_conditional << "$ite_f(" & tff_logic_formula & "," & tff_logic_formula & "," & tff_logic_formula & ")";
        tff_atomic_formula << fof_atomic_formula;
        tff_unary_formula << unary_connective & tff_unitary_formula;
        tff_unary_formula << fof_infix_unary;
        tff_typed_variable << variable & ":" & tff_atomic_type;
        tff_variable << tff_typed_variable | variable;
        tff_variable_list << tff_variable;
        tff_variable_list << tff_variable & "," & tff_variable_list;
        tff_quantified_formula << fof_quantifier & "[" & tff_variable_list & "] :" & tff_unitary_formula;
        tff_unitary_formula << tff_quantified_formula | tff_unary_formula | tff_atomic_formula | tff_conditional | tff_let;
        tff_unitary_formula << "(" & tff_logic_formula & ")";
        tff_and_formula << tff_unitary_formula & "&" & tff_unitary_formula;
        tff_and_formula  << tff_and_formula & "&" & tff_unitary_formula;
        tff_or_formula << tff_unitary_formula & vline & tff_unitary_formula;
        tff_or_formula << tff_or_formula & vline & tff_unitary_formula;
        tff_binary_assoc << tff_or_formula | tff_and_formula;
        tff_binary_nonassoc << tff_unitary_formula & binary_connective & tff_unitary_formula;
        tff_binary_formula << tff_binary_nonassoc | tff_binary_assoc;
        tff_logic_formula << tff_binary_formula | tff_unitary_formula | tff_subtype;
        tff_formula << tff_logic_formula | tff_typed_atom | tff_sequent;
        tfx_logic_formula << thf_logic_formula;
        tfx_formula << tfx_logic_formula | thf_sequent;
        logic_defn_value << "$rigid" | "$flexible" | "$constant" | "$varying" | "$cumulative" | "$decreasing" |
            "$local" | "$global" | "$modal_system_K" | "$modal_system_T" | "$modal_system_D" | "$modal_system_S4" |
            "$modal_system_S5" | "$modal_axiom_K" | "$modal_axiom_T" | "$modal_axiom_B" | "$modal_axiom_D" |
            "$modal_axiom_4" | "$modal_axiom_5";
        logic_defn_value << defined_constant;
        logic_defn_RHS << logic_defn_value | thf_unitary_formula;
        logic_defn_LHS << "$constants" | "$quantification" | "$consequence" | "$modalities";
        logic_defn_LHS << logic_defn_value | thf_top_level_type | name;
        logic_defn_rule << logic_defn_LHS & assignment & logic_defn_RHS;
        thf_formula_list << thf_logic_formula | thf_logic_formula & "," & thf_formula_list;
        thf_tuple << "[" & "]";
        thf_tuple << "[" & thf_formula_list & "]";
        thf_tuple << "{" & "}";
        thf_tuple  << "{" & thf_formula_list & "}";
        thf_sequent << thf_tuple & gentzen_arrow & thf_tuple;
        thf_sequent << "(" & thf_sequent & ")";
        thf_union_type << thf_unitary_type & plus & thf_unitary_type;
        thf_union_type << thf_union_type & plus & thf_unitary_type;
        thf_xprod_type << thf_unitary_type & star & thf_unitary_type;
        thf_xprod_type << thf_xprod_type & star & thf_unitary_type;
        thf_mapping_type << thf_unitary_type & arrow & thf_unitary_type;
        thf_mapping_type << thf_unitary_type & arrow & thf_mapping_type;
        thf_binary_type << thf_mapping_type | thf_xprod_type | thf_union_type;
        thf_apply_type << thf_apply_formula;
        thf_unitary_type << thf_unitary_formula;
        thf_top_level_type << thf_unitary_type | thf_mapping_type | thf_apply_type;
        thf_type_formula << constant & ":" & thf_top_level_type;
        thf_subtype << thf_atom & subtype_sign & thf_atom;
        thf_typeable_formula << thf_atom;
        thf_typeable_formula << "(" & thf_logic_formula & ")";
        thf_type_formula << thf_typeable_formula & ":" & thf_top_level_type;
        thf_arguments << thf_formula_list;
        thf_let_defn_LHS << constant;
        thf_let_defn_LHS << functor & "(" & fof_arguments & ")";
        thf_let_defn_LHS << thf_tuple;
        thf_let_plain_defn << thf_let_defn_LHS & assignment & thf_formula;
        thf_let_quantified_defn << thf_quantification & "(" & thf_let_plain_defn & ")";
        thf_let_defn << thf_let_quantified_defn | thf_let_plain_defn;
        thf_let_defn_list << thf_let_defn;
        thf_let_defn_list << thf_let_defn & "," & thf_let_defn_list;
        thf_let_defns << thf_let_defn;
        thf_let_defns << "[" & thf_let_defn_list & "]";
        thf_let << "$let(" & thf_let_defns & "," & thf_formula & ")";
        thf_let << "$let(" & thf_unitary_formula & "," & thf_formula & ")";
        thf_conditional << "$ite(" & thf_logic_formula & "," & thf_logic_formula & "," & thf_logic_formula & ")";
        thf_conn_term << thf_pair_connective | assoc_connective | thf_unary_connective;
        thf_function << atom;
        thf_function << functor & "(" & thf_arguments & ")";
        thf_function << defined_functor & "(" & thf_arguments & ")";
        thf_function << system_functor & "(" & thf_arguments & ")";
        thf_atom << thf_function | variable | defined_term | thf_conn_term;
        thf_unary_formula << thf_unary_connective & "(" & thf_logic_formula & ")";
        thf_typed_variable << variable & ":" & thf_top_level_type;
        thf_variable << thf_typed_variable | variable;
        thf_variable_list << thf_variable;
        thf_variable_list  << thf_variable & "," & thf_variable_list;
        thf_quantification << thf_quantifier & "[" & thf_variable_list & "] :";
        thf_quantified_formula << thf_quantification & thf_unitary_formula;
        thf_unitary_formula << thf_quantified_formula | thf_unary_formula | thf_atom | thf_conditional | thf_let | thf_tuple;
        thf_unitary_formula << "(" & thf_logic_formula & ")";
        thf_apply_formula << thf_unitary_formula & "@" & thf_unitary_formula;
        thf_apply_formula  << thf_apply_formula & "@" & thf_unitary_formula;
        thf_and_formula << thf_unitary_formula & "&" & thf_unitary_formula;
        thf_and_formula << thf_and_formula & "&" & thf_unitary_formula;
        thf_or_formula << thf_unitary_formula & vline & thf_unitary_formula;
        thf_or_formula  << thf_or_formula & vline & thf_unitary_formula;
        thf_binary_tuple << thf_or_formula | thf_and_formula | thf_apply_formula;
        thf_binary_pair << thf_unitary_formula & thf_pair_connective & thf_unitary_formula;
        thf_binary_formula << thf_binary_pair | thf_binary_tuple | thf_binary_type;
        thf_logic_formula << thf_binary_formula | thf_unitary_formula | thf_type_formula | thf_subtype;
        thf_formula << thf_logic_formula | thf_sequent;
        formula_role << "axiom" | "hypothesis" | "definition" | "assumption" | "lemma" | "theorem" | "corollary" | "conjecture" |
            "negated_conjecture" | "plain" | "type" | "fi_domain" | "fi_functors" | "fi_predicates" | "unknown";
        formula_role << lower_word;
        annotations << "," & source & optional_info;
        cnf_annotated << "cnf(" & name & "," & formula_role & "," & cnf_formula & annotations & ").";
        cnf_annotated << "cnf(" & name & "," & formula_role & "," & cnf_formula & ").";
        fof_annotated << "fof(" & name & "," & formula_role & "," & fof_formula & annotations & ").";
        fof_annotated << "fof(" & name & "," & formula_role & "," & fof_formula & ").";
        tcf_annotated << "tcf(" & name & "," & formula_role & "," & tcf_formula & annotations & ").";
        tcf_annotated << "tcf(" & name & "," & formula_role & "," & tcf_formula & ").";
        tff_annotated << "tff(" & name & "," & formula_role & "," & tff_formula & annotations & ").";
        tff_annotated << "tff(" & name & "," & formula_role & "," & tff_formula & ").";
        tfx_annotated << "tfx(" & name & "," & formula_role & "," & tfx_formula & annotations & ").";
        tfx_annotated << "tfx(" & name & "," & formula_role & "," & tfx_formula & ").";
        thf_annotated << "thf(" & name & "," & formula_role & "," & thf_formula & annotations & ").";
        thf_annotated << "thf(" & name & "," & formula_role & "," & thf_formula & ").";
        tpi_formula << fof_formula;
        tpi_annotated << "tpi(" & name & "," & formula_role & "," & tpi_formula & annotations & ").";
        tpi_annotated << "tpi(" & name & "," & formula_role & "," & tpi_formula & ").";
        annotated_formula << thf_annotated | tfx_annotated | tff_annotated | tcf_annotated | fof_annotated | cnf_annotated | tpi_annotated;
        TPTP_input << annotated_formula | include;
        TPTP_file << *TPTP_input;

        comment << comment_line;// | comment_block;
        comment_line << "%" & *printable_char & '\n';
        //comment_block << "/ *" & not_star_slash & "**" & "/ ";
        //not_star_slash << ([^*] * [*][*] * [^/*])*[^*]*

    }

    const ParsingExpressionGrammar& TPTPGrammar::grammar() const
    {
        return grammar_;
    }

    namespace
    {
        std::string load_file(const std::string& filename)
        {
            std::ifstream t(filename);

            if (!t)
            {
                throw std::runtime_error("TPTPGrammar::load_file: can't open file '" + filename + "'");
            }

            return std::string((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
        }

        class CommentComsumer : public PEGEarleyParser::CommentComsumer
        {
        public:
            bool consume(char ch) override
            {
                if (ch == '%')
                {
                    state_ = comment;
                }
                else if (ch == '\n')
                {
                    state_ = regular;
                    return true;
                }

                return (state_ == comment);
            }

        private:
            enum State
            {
                regular,
                comment
            };

        private:
            State state_ = regular;
        };
    }

    TPTPInput TPTPGrammar::input_from_file(const std::string& filename) const
    {
        const auto text = load_file(filename);
        return input(text);
    }

    TPTPInput TPTPGrammar::input(const std::string& str) const
    {
        PEGEarleyParser parser(grammar_.get_rule("TPTP_file"));
        parser.set_spaces(" \n");
        parser.set_comment_consumer(std::make_shared<CommentComsumer>());
        const auto nt = parser.parse(str);

        if (!nt)
        {
            parser.print_states(str, std::cout);
            throw std::runtime_error("Can't be parsed");
        }
        else
        {
            std::ofstream ofs("tptp_trace.dot");
            toDOT(*nt, ofs);
        }

        TPTPInput result;

        for (const auto& s : nt->symbols())
        {
            const auto statement = s.nonterminal()->nonterminal(0);

            if (statement->name() == "annotated_formula")
            {
                TPTPInput::AnnotatedFormula afl;
                afl.name = statement->nonterminal(0)->nonterminal(1)->nonterminal(0)->terminals(0);
                afl.formula_role = statement->nonterminal(0)->nonterminal(3)->terminals(0);
                afl.formula = statement->nonterminal(0)->nonterminal(5);
                result.formulas.emplace_back(afl);
            }
            else if (statement->name() == "include")
            {
                std::string incl = statement->nonterminal(1)->terminals(0);

                if (incl.length() > 1 && incl.front() == '\'' && incl.back() == '\'')
                {
                    incl = incl.substr(1, incl.length() - 2);
                }

                result.includes.insert(incl);
            }
        }

        return result;
    }
}
