#include "prover/ConjunctionFormula.h"
#include "prover/FormulaVisitor.h"

namespace prime
{
    ConjunctionFormula::ConjunctionFormula(const FormulaPtr& arg1, const FormulaPtr& arg2)
        : arg1_(arg1), arg2_(arg2)
    {}

    FormulaPtr ConjunctionFormula::clone() const
    {
        return std::make_shared<ConjunctionFormula>(arg1_, arg2_);
    }

    void ConjunctionFormula::visit(FormulaVisitor& visitor) const
    {
        visitor.handle(*this);
    }

    const FormulaPtr& ConjunctionFormula::arg1() const
    {
        return arg1_;
    }

    const FormulaPtr& ConjunctionFormula::arg2() const
    {
        return arg2_;
    }
}
