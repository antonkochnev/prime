#include "prover/Variable.h"
#include "prover/FormulaVisitor.h"

namespace prime
{
    Variable::Variable(const std::string& name)
        : name_(name)
    {}

    FormulaPtr Variable::clone() const
    {
        return std::make_shared<Variable>(name_);
    }

    void Variable::visit(FormulaVisitor& visitor) const
    {
        visitor.handle(*this);
    }

    const std::string& Variable::name() const
    {
        return name_;
    }
}
