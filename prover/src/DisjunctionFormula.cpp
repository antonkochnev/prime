#include "prover/DisjunctionFormula.h"
#include "prover/FormulaVisitor.h"

namespace prime
{
    DisjunctionFormula::DisjunctionFormula(const FormulaPtr& arg1, const FormulaPtr& arg2)
        : arg1_(arg1), arg2_(arg2)
    {}

    FormulaPtr DisjunctionFormula::clone() const
    {
        return std::make_shared<DisjunctionFormula>(arg1_, arg2_);
    }

    void DisjunctionFormula::visit(FormulaVisitor& visitor) const
    {
        visitor.handle(*this);
    }

    const FormulaPtr& DisjunctionFormula::arg1() const
    {
        return arg1_;
    }

    const FormulaPtr& DisjunctionFormula::arg2() const
    {
        return arg2_;
    }
}
