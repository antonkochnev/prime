#include "prover/Prover.h"

#include <chrono>

namespace prime
{
    Prover::Prover(ProverStrategy& strategy)
        : strategy_(strategy)
    {
    }

    ProofPtr Prover::prove(const Sequent& sequent)
    {
        const auto begin_time = std::chrono::system_clock::now();

        auto proof = std::make_unique<Proof>(sequent);
        prove_tasks_.push(std::make_shared<ProveTask>(proof->root_node));

        while (!prove_tasks_.empty())
        {
            const auto task = prove_tasks_.front();
            prove_tasks_.pop();
            do_prove_task(*task);
        }

        const auto end_time = std::chrono::system_clock::now();
        proof->stats.elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - begin_time).count();
        return proof;
    }

    void Prover::do_prove_task(const ProveTask& task)
    {
        strategy_.apply(task.node);

        for (auto& n : task.node.nodes)
        {
            prove_tasks_.emplace(std::make_shared<ProveTask>(n));
        }
    }
}
