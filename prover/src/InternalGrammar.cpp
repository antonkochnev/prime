#include "prover/InternalGrammar.h"

#include <sstream>
#include <stack>

#include "grammar/NonterminalUtils.h"
#include "grammar/PEGEarleyParser.h"

#include "prover/NotFormula.h"
#include "prover/ConjunctionFormula.h"
#include "prover/DisjunctionFormula.h"
#include "prover/ImplicationFormula.h"
#include "prover/PredicateFormula.h"

namespace prime
{
    namespace
    {
        const std::string VARIABLE_NAME = "variable";
        const std::string FORMULA_NAME = "formula";
        const std::string NEGATION_NAME = "negation";
        const std::string CONJUNCTION_NAME = "conjunction";
        const std::string DISJUNCTION_NAME = "disjunction";
        const std::string IMPLICATION_NAME = "implication";
        const std::string SEQUENT_NAME = "sequent";
    }

    InternalGrammar::InternalGrammar()
    :
        grammar_("InternalGrammar")
    {
        PEG::CharacterClass letter(grammar_, "letter");
        letter << "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        PEG::CharacterClass digit(grammar_, "digit");
        digit << "0123456789";

        PEG::Token word(grammar_, "word");
        word << letter;
        word << word & letter;
        word << word & digit;

        PEG::ProductionRule variable(grammar_, VARIABLE_NAME);
        variable << word;

        PEG::ProductionRule formula(grammar_, FORMULA_NAME);
        formula << variable;
        formula << '(' & formula & ')';

        PEG::ProductionRule negation(grammar_, NEGATION_NAME);
        negation << '~' & formula;

        PEG::ProductionRule conjunction(grammar_, CONJUNCTION_NAME);
        conjunction << formula & '&' & formula;

        PEG::ProductionRule disjunction(grammar_, DISJUNCTION_NAME);
        disjunction << formula & '|' & formula;

        PEG::ProductionRule implication(grammar_, IMPLICATION_NAME);
        implication << formula & '=' & '>' & formula;

        formula << negation | conjunction | disjunction | implication;
        formulaExpr_ = formula.expr();

        PEG::ProductionRule formula_list(grammar_, "formula_list");
        formula_list << formula;
        formula_list << formula_list & ',' & formula;

        PEG::ProductionRule sequent(grammar_, SEQUENT_NAME);
        sequent << "|-" & formula_list;
        sequent << formula_list & "|-";
        sequent << formula_list & "|-" & formula_list;
        sequentExpr_ = sequent.expr();
    }

    const ParsingExpressionGrammar& InternalGrammar::grammar() const
    {
        return grammar_;
    }

    Sequent InternalGrammar::sequent(const std::string& str) const
    {
        PEGEarleyParser parser(*sequentExpr_);
        parser.set_spaces(" ");
        const auto nt = parser.parse(str);

        if (!nt)
        {
            std::ostringstream oss;
            oss << "Unparseable sequent: '" << str << "'\n\n";
            //parser.print_states(str, oss);
            throw std::runtime_error(oss.str());
        }

        SequentConverter converter;
        return converter.convert(*nt);
    }

    FormulaPtr InternalGrammar::formula(const char* str) const
    {
        PEGEarleyParser parser(*formulaExpr_);
        parser.set_spaces(" ");
        const auto nt = parser.parse(str);

        if (!nt)
        {
            std::ostringstream oss;
            oss << "Unparseable formula: '" << str << "'\n\n";
            //parser.print_states(str, oss);
            throw std::runtime_error(oss.str());
        }

        FormulaConverter converter;
        return converter.convert(*nt);
    }

    std::string InternalGrammar::print(const Formula& fl)
    {
        FormulaConverter converter;
        const auto nt = converter.convert(fl);
        std::ostringstream oss;
        prime::print(*nt, oss);
        return oss.str();
    }

    std::string InternalGrammar::print(const Sequent& sequent)
    {
        SequentConverter converter;
        const auto nt = converter.convert(sequent);
        std::ostringstream oss;
        prime::print(*nt, oss);
        return oss.str();
    }

    /*
     * InternalGrammar::FormulaConverter
     */
    FormulaPtr InternalGrammar::FormulaConverter::convert(const Nonterminal& nt)
    {
        FormulaPtr formula;

        if (nt.name() == VARIABLE_NAME)
        {
            formula = build_variable_(nt[0].terminals());
        }
        else if (nt.name() == FORMULA_NAME)
        {
            formula = build_formula_(*nt[nt.size() == 1 ? 0 : 1].nonterminal());
        }

        return formula;
    }

    namespace
    {
        class FormulaPrinter : public FormulaVisitor
        {
        public:

            NonterminalPtr result()
            {
                NonterminalPtr res;

                while (!stack_.empty())
                {
                    res = stack_.top();
                    stack_.pop();
                }

                return res;
            }

            virtual void handle(const NotFormula& fl) override
            {
                addNode_();
                add_('~');
                fl.arg()->visit(*this);
            }

            virtual void handle(const Variable& fl) override
            {
                if (stack_.empty())
                {
                    addNode_();
                }

                add_(fl.name());
            }

            virtual void handle(const ConjunctionFormula& fl) override
            {
                addNode_();
                fl.arg1()->visit(*this);
                add_('&');
                fl.arg2()->visit(*this);
            }

            virtual void handle(const DisjunctionFormula& fl) override
            {
                addNode_();
                fl.arg1()->visit(*this);
                add_('|');
                fl.arg2()->visit(*this);
            }

            virtual void handle(const ImplicationFormula& fl) override
            {
                addNode_();
                fl.arg1()->visit(*this);
                add_("=>");
                fl.arg2()->visit(*this);
            }

            virtual void handle(const PredicateFormula& fl) override
            {
                addNode_();
                add_(fl.functor());
                add_('(');
                const auto& args = fl.args();

                for (std::size_t i = 0; i < args.size(); ++i)
                {
                    if (i)
                    {
                        add_(',');
                    }

                    args[i]->visit(*this);
                }
            }

        private:
            NonterminalPtr res_;
            std::stack<NonterminalPtr> stack_;

        private:
            void addNode_()
            {
                auto node = std::make_shared<Nonterminal>();

                if (!stack_.empty())
                {
                    stack_.top()->add(node);
                }

                stack_.push(node);
            }

            void add_(const std::string& str)
            {
                stack_.top()->add(str);
            }

            void add_(char ch)
            {
                stack_.top()->add(ch);
            }
        };
    }

    NonterminalPtr InternalGrammar::FormulaConverter::convert(const Formula& fl)
    {
        FormulaPrinter printer;
        fl.visit(printer);
        return printer.result();
    }

    FormulaPtr InternalGrammar::FormulaConverter::build_formula_(const Nonterminal& nt)
    {
        FormulaPtr formula;

        if (nt.name() == VARIABLE_NAME)
        {
            if (nt.size() != 1 || !nt[0].is_terminal())
            {
                std::ostringstream oss;
                oss << "Wrong nonterminal for variable construction: ";
                prime::print(nt, oss);
                throw std::runtime_error(oss.str());
            }

            formula = build_variable_(nt[0].terminals());
        }
        else if (nt.name() == FORMULA_NAME)
        {
            formula = build_formula_(*nt[nt.size() == 1 ? 0 : 1].nonterminal());
        }
        else if (nt.name() == NEGATION_NAME)
        {
            formula = std::make_shared<NotFormula>(build_formula_(*nt[1].nonterminal()));
        }
        else if (nt.name() == CONJUNCTION_NAME)
        {
            formula = std::make_shared<ConjunctionFormula>(build_formula_(*nt[0].nonterminal()), build_formula_(*nt[2].nonterminal()));
        }
        else if (nt.name() == DISJUNCTION_NAME)
        {
            formula = std::make_shared<DisjunctionFormula>(build_formula_(*nt[0].nonterminal()), build_formula_(*nt[2].nonterminal()));
        }
        else if (nt.name() == IMPLICATION_NAME)
        {
            formula = std::make_shared<ImplicationFormula>(build_formula_(*nt[0].nonterminal()), build_formula_(*nt[2].nonterminal()));
        }

        return formula;
    }

    const std::shared_ptr<Variable>& InternalGrammar::FormulaConverter::build_variable_(const std::string& name)
    {
        auto it = variables_.find(name);

        if (it == variables_.end())
        {
            it = variables_.emplace(name, std::make_shared<Variable>(name)).first;
        }

        return it->second;
    }

    /*
     * InternalGrammar::SequentConverter
     */
    Sequent InternalGrammar::SequentConverter::convert(const Nonterminal& nt)
    {
        if (nt.name() != SEQUENT_NAME)
        {
            throw std::logic_error("invalid symbol name");
        }

        Sequent sequent;

        if (nt.size() == 3)
        {
            build_formula_list_(*nt[0].nonterminal(), sequent.assumptions);
            build_formula_list_(*nt[2].nonterminal(), sequent.conjecture);
        }
        else if (nt[0].is_terminal())
        {
            build_formula_list_(*nt[1].nonterminal(), sequent.conjecture);
        }
        else
        {
            build_formula_list_(*nt[0].nonterminal(), sequent.assumptions);
        }

        return sequent;
    }

    void InternalGrammar::SequentConverter::build_formula_list_(const Nonterminal& nt, std::vector<FormulaPtr>& formula_list)
    {
        for (const auto& s : nt.symbols())
        {
            InternalGrammar::FormulaConverter builder;
            const auto formula = builder.convert(*s.nonterminal());
            formula_list.push_back(formula);
        }
    }

    namespace
    {
        void add(NonterminalPtr& res, const std::vector<FormulaPtr>& formulas)
        {
            InternalGrammar::FormulaConverter formulaConverter;

            for (std::size_t i = 0; i < formulas.size(); ++i)
            {
                if (i)
                {
                    res->add(',');
                }

                const auto& fl = formulas[i];
                res->add(formulaConverter.convert(*fl));
            }
        }
    }

    NonterminalPtr InternalGrammar::SequentConverter::convert(const Sequent& sequent)
    {
        auto res = std::make_shared<Nonterminal>();
        add(res, sequent.assumptions);
        res->add('|');
        res->add('-');
        add(res, sequent.conjecture);
        return res;
    }
}
