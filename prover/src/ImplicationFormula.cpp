#include "prover/ImplicationFormula.h"
#include "prover/FormulaVisitor.h"

namespace prime
{
    ImplicationFormula::ImplicationFormula(const FormulaPtr& arg1, const FormulaPtr& arg2)
        : arg1_(arg1), arg2_(arg2)
    {}

    FormulaPtr ImplicationFormula::clone() const
    {
        return std::make_shared<ImplicationFormula>(arg1_, arg2_);
    }

    void ImplicationFormula::visit(FormulaVisitor& visitor) const
    {
        visitor.handle(*this);
    }

    const FormulaPtr& ImplicationFormula::arg1() const
    {
        return arg1_;
    }

    const FormulaPtr& ImplicationFormula::arg2() const
    {
        return arg2_;
    }
}
