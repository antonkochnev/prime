#include "prover/FormulaWalker.h"

#include "prover/NotFormula.h"
#include "prover/Variable.h"
#include "prover/ConjunctionFormula.h"
#include "prover/DisjunctionFormula.h"
#include "prover/ImplicationFormula.h"

namespace prime
{
    void FormulaWalker::handle(const NotFormula& fl)
    {
        nodeNotFormula(fl);
        fl.arg()->visit(*this);
    }

    void FormulaWalker::handle(const Variable& fl)
    {
        nodeVariable(fl);
    }

    void FormulaWalker::handle(const ConjunctionFormula& fl)
    {
        nodeConjunctionFormula(fl);
        fl.arg1()->visit(*this);
        fl.arg2()->visit(*this);
    }

    void FormulaWalker::handle(const DisjunctionFormula& fl)
    {
        nodeDisjunctionFormula(fl);
        fl.arg1()->visit(*this);
        fl.arg2()->visit(*this);
    }

    void FormulaWalker::handle(const ImplicationFormula& fl)
    {
        nodeImplicationFormula(fl);
        fl.arg1()->visit(*this);
        fl.arg2()->visit(*this);
    }
}
