#include "prover/TPTPSyntaxBNFParser.h"

#include <cctype>
#include <iostream>
#include <fstream>
#include <set>

namespace prime
{
    namespace
    {
        enum class ExpressionType
        {
            RegularRule, // ::=
            SemanticRule, // :==
            Token, // ::-
            CharacterClass // :::
        };

        enum class State
        {
            raw,
            name
        };

        struct Element
        {
            std::string text;
            State type;
        };

        struct Expression
        {
            std::string name;
            ExpressionType type;
            std::vector<Element> elements;
        };

        std::string trim(std::string& str)
        {
            str.erase(0, str.find_first_not_of(' '));
            str.erase(str.find_last_not_of(' ') + 1);
            return str;
        }

        char consume_char(const std::string& str, std::size_t offset, std::size_t& pos)
        {
            if (str[offset] == '\\')
            {
                pos = offset + 1;

                if (str[pos] == '\\')
                {
                    ++pos;
                    return '\\';
                }

                std::string num;

                while (std::isdigit(str[pos]))
                {
                    num += str[pos];
                    ++pos;
                }

                return static_cast<char>(std::atoi(num.c_str()));
            }

            pos = offset + 1;
            return str[offset];
        }

        std::size_t add_char_range(std::size_t i, const std::string& str, Expression& expr)
        {
            while (str[i] != ']')
            {
                const char b = consume_char(str, i, i);

                if (str[i] != '-' || str[i + 1] == ']')
                {
                    expr.elements.push_back({ std::string{ b }, State::raw });
                }
                else
                {
                    ++i;
                    const char e = consume_char(str, i, i);

                    for (int ch = b; ch <= e; ++ch)
                    {
                        expr.elements.push_back({ std::string{ static_cast<char>(ch) }, State::raw });
                    }

                    break;
                }
            }

            return i;
        }

        void parse_character_class(const std::string& str, Expression& expr)
        {
            if (str == ".")
            {
                for (int i = 32; i <= 126; ++i)
                {
                    expr.elements.push_back({ std::string{ static_cast<char>(i) }, State::raw });
                }

                return;
            }
            else if (str[0] == '[')
            {
                add_char_range(1, str, expr);
            }
            else if (str[0] == '(')
            {
                expr.elements.push_back({ str.substr(1, str.length() - 2), State::raw });
                /*std::size_t i = 1;

                if (str[i] == '[')
                {
                    ++i;

                    while (str[i] != ']')
                    {
                        i = add_char_range(i, str, expr);
                    }
                }
                else if (str[i] == '<')
                {
                    expr.elements.push_back({ str.substr(1, str.length() - 2), State::raw });
                }*/
            }
        }

        void parse_regular_rule(const std::string& str, Expression& expr)
        {
            // tpi(<name>,<formula_role>,<tpi_formula> <annotations>).
            std::string token;
            State state = State::raw;

            for (std::size_t i = 0; i < str.length(); ++i)
            {
                const auto ch = str[i];

                if (state == State::raw)
                {
                    if (ch == '<' && i + 1 < str.length() && std::isalpha(str[i + 1]))
                    {
                        token = trim(token);

                        if (!token.empty())
                        {
                            expr.elements.push_back({ token, State::raw });
                            token.clear();
                        }

                        state = State::name;
                    }

                    token += ch;
                }
                else if (state == State::name)
                {
                    token += ch;

                    if (ch == '>')
                    {
                        expr.elements.push_back({ trim(token), State::name });
                        token.clear();
                        state = State::raw;
                    }
                }
            }

            if (state == State::name)
            {
                throw std::runtime_error(str + " : state == State::name at the end");
            }

            token = trim(token);

            if (!token.empty())
            {
                expr.elements.push_back({ token, State::raw });
            }
        }

        void parse_token(const std::string& str, Expression& expr)
        {
            parse_regular_rule(str, expr);
        }

        void parse_semantic_rule(const std::string& str, Expression& expr)
        {
            parse_regular_rule(str, expr);
        }

        Expression parse_expression(const std::string& str)
        {
            Expression expr;
            const auto name_end = str.find('>');

            if (name_end == std::string::npos)
            {
                throw std::runtime_error(str + " : name_end == std::string::npos");
            }

            expr.name = str.substr(1, name_end - 1);
            const auto type_pos = str.find(':', name_end);

            if (type_pos == std::string::npos)
            {
                throw std::runtime_error(str + " : type_pos == std::string::npos");
            }

            const auto type = str.substr(type_pos, 3);
            const auto defn = (type_pos + 4 < str.length() ? str.substr(type_pos + 4, str.length()) : std::string());

            if (type == "::=")
            {
                expr.type = ExpressionType::RegularRule;
                parse_regular_rule(defn, expr);
            }
            else if (type == ":==")
            {
                expr.type = ExpressionType::SemanticRule;
                parse_semantic_rule(defn, expr);
            }
            else if (type == "::-")
            {
                expr.type = ExpressionType::Token;
                parse_token(defn, expr);
            }
            else if (type == ":::")
            {
                expr.type = ExpressionType::CharacterClass;
                parse_character_class(defn, expr);
            }

            return expr;
        }

        void parse_expressions(const std::string& filename, std::vector<Expression>& expressions)
        {
            std::ifstream ifs(filename);

            if (!ifs)
            {
                throw std::runtime_error("can't open file " + filename);
            }

            std::string line;
            std::string expression;

            while (std::getline(ifs, line))
            {
                if (!line.empty() && line[0] != '%')
                {
                    if (line[0] == '<')
                    {
                        if (!expression.empty())
                        {
                            expressions.push_back(parse_expression(expression));
                        }

                        expression = line;
                    }
                    else
                    {
                        expression += line;
                    }
                }
            }
        }
    }

    std::shared_ptr<Grammar> TPTPSyntaxBNFParser::parse(const std::string& filename)
    {
        std::vector<Expression> expressions;
        parse_expressions(filename, expressions);
        const auto grammar = std::make_shared<Grammar>("TPTPGrammar");

        std::set<std::string> names;

        for (auto ri = expressions.rbegin(); ri != expressions.rend(); ++ri)
        {
            if (names.insert(ri->name).second)
            {
                if (ri->type == ExpressionType::RegularRule || ri->type == ExpressionType::SemanticRule || ri->type == ExpressionType::Token)
                {
                    if (ri->type == ExpressionType::Token)
                    {
                        std::cout << "TOK(" << ri->name << ");\n";
                    }
                    else
                    {
                        std::cout << "DEF(" << ri->name << ");\n";
                    }
                }
                else
                {
                    std::cout << "CC(" << ri->name << ");\n";
                }
            }
        }

        for (auto ri = expressions.rbegin(); ri != expressions.rend(); ++ri)
        {
            if (ri->elements.empty())
            {
                continue;
            }

            std::cout << ri->name << " <<";

            if (ri->type == ExpressionType::RegularRule || ri->type == ExpressionType::SemanticRule || ri->type == ExpressionType::Token)
            {
                bool seqFlag = false;

                for (std::size_t i = 0; i < ri->elements.size(); ++i)
                {
                    const auto& e = ri->elements[i];

                    if (e.type == State::name)
                    {
                        if (seqFlag)
                        {
                            std::cout << " &";
                        }

                        if (i + 1 < ri->elements.size() && ri->elements[i + 1].type == State::raw && ri->elements[i + 1].text == "*")
                        {
                            std::cout << " *";
                            ++i;
                        }
                        else
                        {
                            std::cout << ' ';
                        }

                        std::cout << e.text.substr(1, e.text.length() - 2);
                        seqFlag = true;
                    }
                    else if (e.type == State::raw && e.text == "|")
                    {
                        std::cout << " |";
                        seqFlag = false;
                    }
                    else
                    {
                        if (seqFlag)
                        {
                            std::cout << " &";
                        }

                        std::cout << ' ' << '"' << e.text << '"';
                        seqFlag = true;
                    }
                }

                std::cout << ";\n";
            }
            else if (ri->type == ExpressionType::CharacterClass)
            {
                std::cout << " \"";

                for (const auto e : ri->elements)
                {
                    std::string normalized = e.text;

                    if (normalized == "\"")
                    {
                        normalized = "\\\"";
                    }
                    else if (normalized == "\\")
                    {
                        normalized = "\\\\";
                    }

                    std::cout << normalized;
                }

                std::cout << "\";\n";
            }
        }

        return grammar;
    }
}
