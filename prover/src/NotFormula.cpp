#include "prover/NotFormula.h"
#include "prover/FormulaVisitor.h"

namespace prime
{
    NotFormula::NotFormula(const FormulaPtr& arg)
        : arg_(arg)
    {}

    FormulaPtr NotFormula::clone() const
    {
        return std::make_shared<NotFormula>(arg_);
    }

    const FormulaPtr& NotFormula::arg() const
    {
        return arg_;
    }

    void NotFormula::visit(FormulaVisitor& visitor) const
    {
        visitor.handle(*this);
    }
}
