#include "prover/VariableReplaceRule.h"

#include <sstream>

#include "prover/FormulaWalker.h"
#include "prover/InternalGrammar.h"

namespace prime
{
    namespace
    {
        class Replacer : public FormulaWalker
        {
        public:
            virtual void nodeVariable(const Variable& fl) override
            {

            }
        };
    }

    VariableReplaceRule::VariableReplaceRule(const VariablePtr& src, const FormulaPtr& trg)
        : src_(src), trg_(trg)
    {}

    std::string VariableReplaceRule::description() const
    {
        std::ostringstream oss;
        oss << "VariableReplace: '" << src_->name() << "' to '" << InternalGrammar::print(*trg_) << "'";
        return oss.str();
    }

    Sequent VariableReplaceRule::apply(const Sequent& src)
    {
        Sequent res;
        return res;
    }
}
