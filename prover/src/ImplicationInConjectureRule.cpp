#include "prover/ImplicationInConjectureRule.h"

#include <sstream>

#include "prover/ImplicationFormula.h"

namespace prime
{
    ImplicationInConjectureRule::ImplicationInConjectureRule(std::size_t inx)
        : inx_(inx)
    {}

    std::string ImplicationInConjectureRule::description() const
    {
        std::ostringstream oss;
        oss << "ImplicationInConjecture " << inx_;
        return oss.str();
    }

    Sequent ImplicationInConjectureRule::apply(const Sequent& src)
    {
        const auto fl = static_cast<const ImplicationFormula*>(src.conjecture[inx_].get());
        Sequent res = src;
        res.conjecture.erase(inx_);
        res.assumptions.emplace_back(fl->arg1());
        res.conjecture.emplace_back(fl->arg2());
        return res;
    }
}
