#include "prover/TPTPInput.h"

#include <iostream>
#include <map>

#include "prover/TPTPGrammar.h"
#include "prover/NotFormula.h"
#include "prover/PredicateFormula.h"
#include "prover/Variable.h"

namespace prime
{
    namespace
    {
        class FormulaMaker
        {
        public:
            std::map<std::string, std::shared_ptr<Variable>> variables;

        public:
            FormulaPtr make(const Nonterminal& nt)
            {
                FormulaPtr fl;

                if (nt.name() == "cnf_formula")
                {
                    fl = cnf_formula(*nt.nonterminal(0));
                }

                return fl;
            }

            FormulaPtr cnf_formula(const Nonterminal& nt)
            {
                if (nt.size() == 1 && nt.is_nonterminal(0))
                {
                    return disjunction(*nt.nonterminal(0));
                }
                else if (nt.size() == 3 &&
                    nt.is_terminal(0) && nt.terminals(0) == "(" &&
                    nt.is_nonterminal(1) &&
                    nt.is_terminal(2) && nt.terminals(2) == ")")
                {
                    return disjunction(*nt.nonterminal(1));
                }

                throw std::runtime_error("FormulaMaker::cnf_formula: incorrect symbol");
            }

            FormulaPtr disjunction(const Nonterminal& nt)
            {
                if (nt.size() == 1 && nt.is_nonterminal(0))
                {
                    return literal(*nt.nonterminal(0));
                }

                throw std::runtime_error("FormulaMaker::disjunction: incorrect symbol");
            }

            FormulaPtr literal(const Nonterminal& nt)
            {
                if (nt.size() == 1 && nt.is_nonterminal(0))
                {
                    return fof_atomic_formula(*nt.nonterminal(0));
                }
                else if (nt.size() == 2 && nt.is_terminal(0) && nt.terminals(0) == "~")
                {
                    const auto fl = fof_atomic_formula(*nt.nonterminal(1));
                    return std::make_shared<NotFormula>(fl);
                }

                throw std::runtime_error("FormulaMaker::literal: incorrect symbol");
            }

            FormulaPtr fof_atomic_formula(const Nonterminal& nt)
            {
                if (nt.size() == 1 && nt.is_nonterminal(0))
                {
                    const auto& s = *nt.nonterminal(0);

                    if (s.name() == "fof_plain_atomic_formula")
                    {
                        return fof_plain_atomic_formula(s);
                    }
                }

                throw std::runtime_error("FormulaMaker::fof_atomic_formula: incorrect symbol");
            }

            FormulaPtr fof_plain_atomic_formula(const Nonterminal& nt)
            {
                if (nt.size() == 1 && nt.is_nonterminal(0))
                {
                    const auto& s = *nt.nonterminal(0);

                    if (s.name() == "fof_plain_term")
                    {
                        return fof_plain_term(s);
                    }
                }

                throw std::runtime_error("FormulaMaker::fof_plain_atomic_formula: incorrect symbol");
            }

            FormulaPtr fof_plain_term(const Nonterminal& nt)
            {
                if (nt.size() == 4 && nt.is_nonterminal(0) && nt.nonterminal(0)->name() == "functor")
                {
                    const auto& functor_name = functor(*nt.nonterminal(0));

                    if (nt.is_nonterminal(2))
                    {
                        std::vector<FormulaPtr> args;
                        fof_arguments(*nt.nonterminal(2), args);
                        return std::make_shared<PredicateFormula>(functor_name, args);
                    }
                }
                else if (nt.is_nonterminal(0) && nt.nonterminal(0)->name() == "constant")
                {
                    return constant(*nt.nonterminal(0));
                }

                throw std::runtime_error("FormulaMaker::fof_plain_term: incorrect symbol");
            }

            FormulaPtr constant(const Nonterminal& nt)
            {
                if (nt.size() == 1 && nt.is_terminal(0))
                {
                    const auto& name = functor(*nt.nonterminal(0));
                    return variables.emplace(name, std::make_shared<Variable>(name)).first->second;
                }

                throw std::runtime_error("FormulaMaker::constant: incorrect symbol");
            }

            const std::string& functor(const Nonterminal& nt)
            {
                if (nt.size() == 1 && nt.is_nonterminal(0))
                {
                    return atomic_word(*nt.nonterminal(0));
                }

                throw std::runtime_error("FormulaMaker::functor: incorrect symbol");
            }

            const std::string& atomic_word(const Nonterminal& nt)
            {
                if (nt.size() == 1 && nt.is_terminal(0))
                {
                    return nt.terminals(0);
                }

                throw std::runtime_error("FormulaMaker::atomic_word: incorrect symbol");
            }

            void fof_arguments(const Nonterminal& nt, std::vector<FormulaPtr>& args)
            {
                if (nt.is_nonterminal(0))
                {
                    args.push_back(fof_term(*nt.nonterminal(0)));
                }

                if (nt.is_nonterminal(2))
                {
                    fof_arguments(*nt.nonterminal(2), args);
                }
            }

            FormulaPtr fof_term(const Nonterminal& nt)
            {
                if (nt.size() == 1 && nt.is_nonterminal(0))
                {
                    const auto& s = *nt.nonterminal(0);

                    if (s.name() == "fof_function_term")
                    {
                        return fof_function_term(s);
                    }
                }

                throw std::runtime_error("FormulaMaker::fof_term: incorrect symbol");
            }

            FormulaPtr fof_function_term(const Nonterminal& nt)
            {
                if (nt.size() == 1 && nt.is_nonterminal(0))
                {
                    const auto& s = *nt.nonterminal(0);

                    if (s.name() == "fof_plain_term")
                    {
                        return fof_plain_term(s);
                    }
                }

                throw std::runtime_error("FormulaMaker::fof_function_term: incorrect symbol");
            }
        };
    }

    Sequent TPTPInput::sequent(const std::string& path) const
    {
        Sequent s;
        TPTPGrammar tptp;
        FormulaMaker formula_maker;

        std::set<std::string> files = includes;
        std::set<std::string> processed_files;

        while (!files.empty())
        {
            const auto file = *files.begin();
            processed_files.insert(file);
            files.erase(files.begin());
            std::cout << "start parse " << file << std::endl;
            const auto inclInput = tptp.input_from_file(path + "/" + file);
            std::cout << "finish parse " << file << std::endl;

            for (const auto& nt : inclInput.formulas)
            {
                s.assumptions.push_back(formula_maker.make(*nt.formula));
            }
        }

        for (const auto& nt : formulas)
        {
            s.assumptions.push_back(formula_maker.make(*nt.formula));
        }

        return s;
    }
}
