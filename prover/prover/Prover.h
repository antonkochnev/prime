#pragma once

#include <queue>

#include "Proof.h"
#include "ProverStrategy.h"

namespace prime
{
    class Prover
    {
    public:
        Prover(ProverStrategy& strategy);
        ProofPtr prove(const Sequent& sequent);

    private:
        class ProveTask
        {
        public:
            ProofNode& node;

        public:
            ProveTask(ProofNode& n)
            :
                node(n)
            {}
        };

        typedef std::shared_ptr<ProveTask> ProveTaskPtr;

    private:
        ProverStrategy& strategy_;
        std::queue<ProveTaskPtr> prove_tasks_;

    private:
        void do_prove_task(const ProveTask& task);
    };
}
