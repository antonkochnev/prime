#pragma once

#include <chrono>
#include <list>
#include <memory>
#include <string>

#include "InferenceRule.h"
#include "Sequent.h"

namespace prime
{
    class ProofNode
    {
    public:
        const Sequent sequent;
        InferenceRulePtr rule;
        std::list<ProofNode> nodes;
        std::string comment;
        std::size_t num = 0;

    public:
        ProofNode(Sequent&& s)
            : sequent(std::move(s))
        {}

        ProofNode(const Sequent& s)
            : sequent(s)
        {}
    };

    struct ProofStats
    {
        std::chrono::milliseconds::rep elapsed = 0;
    };

    class Proof
    {
    public:
        ProofNode root_node;
        std::string description;
        ProofStats stats;

    public:
        Proof(const Sequent& sequent)
            : root_node(sequent)
        {}
    };

    typedef std::unique_ptr<Proof> ProofPtr;
}
