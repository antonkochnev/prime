#pragma once

#include "InferenceRule.h"
#include "Variable.h"

namespace prime
{
    class VariableReplaceRule : public InferenceRule
    {
    public:
        VariableReplaceRule(const VariablePtr& src, const FormulaPtr& trg);
        virtual std::string description() const override;
        virtual Sequent apply(const Sequent& src) override;

    private:
        const VariablePtr src_;
        const FormulaPtr trg_;
    };
}
