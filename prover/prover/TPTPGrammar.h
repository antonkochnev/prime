#pragma once

#include "grammar/ParsingExpressionGrammar.h"

#include "Sequent.h"
#include "TPTPInput.h"

namespace prime
{
    class TPTPGrammar
    {
    public:
        TPTPGrammar();
        const ParsingExpressionGrammar& grammar() const;
        TPTPInput input_from_file(const std::string& filename) const;
        TPTPInput input(const std::string& str) const;

    private:
        ParsingExpressionGrammar grammar_;
    };
}
