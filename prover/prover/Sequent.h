#pragma once

#include <vector>

#include "Formula.h"

namespace prime
{
    class FormulasList : public std::vector<FormulaPtr>
    {
    public:
        void erase(std::size_t inx)
        {
            auto it = begin();
            std::advance(it, inx);
            std::vector<FormulaPtr>::erase(it);
        }
    };

    class Sequent
    {
    public:
        FormulasList assumptions;
        FormulasList conjecture;
    };
}
