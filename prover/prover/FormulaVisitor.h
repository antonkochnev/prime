#pragma once

namespace prime
{
    class NotFormula;
    class Variable;
    class ConjunctionFormula;
    class DisjunctionFormula;
    class ImplicationFormula;
    class PredicateFormula;

    class FormulaVisitor
    {
    public:
        virtual ~FormulaVisitor() = default;

        virtual void handle(const NotFormula& fl) = 0;
        virtual void handle(const Variable& fl) = 0;
        virtual void handle(const ConjunctionFormula& fl) = 0;
        virtual void handle(const DisjunctionFormula& fl) = 0;
        virtual void handle(const ImplicationFormula& fl) = 0;
        virtual void handle(const PredicateFormula& fl) = 0;
    };
}

