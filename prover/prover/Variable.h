#pragma once

#include <string>

#include "Formula.h"

namespace prime
{
    class Variable : public Formula
    {
    public:
        Variable(const std::string& name);
        virtual FormulaPtr clone() const override;
        virtual void visit(FormulaVisitor& visitor) const override;
        const std::string& name() const;

    private:
        const std::string name_;
    };

    typedef std::shared_ptr<Variable> VariablePtr;
}
