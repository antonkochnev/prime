#pragma once

#include <vector>

#include "Formula.h"

namespace prime
{
    class PredicateFormula : public Formula
    {
    public:
        PredicateFormula(const std::string& functor, const std::vector<FormulaPtr>& args);
        virtual FormulaPtr clone() const override;
        virtual void visit(FormulaVisitor& visitor) const override;
        const std::vector<FormulaPtr>& args() const;
        const std::string& functor() const;

    private:
        std::string functor_;
        std::vector<FormulaPtr> args_;
    };
}
