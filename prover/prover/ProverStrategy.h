#pragma once

#include "Proof.h"

namespace prime
{
    class ProverStrategy
    {
    public:
        virtual ~ProverStrategy() = default;
        virtual void apply(ProofNode& node) = 0;
    };
}
