#pragma once

#include "InferenceRule.h"

namespace prime
{
    class NotInAssumptionsRule : public InferenceRule
    {
    public:
        NotInAssumptionsRule(std::size_t inx);
        virtual std::string description() const override;
        virtual Sequent apply(const Sequent& src) override;

    private:
        std::size_t inx_;
    };
}
