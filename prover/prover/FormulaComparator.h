#pragma once

#include "Formula.h"

namespace prime
{
    class FormulaComparator
    {
    public:
        bool equals(const Formula& arg1, const Formula& arg2);
    };
}
