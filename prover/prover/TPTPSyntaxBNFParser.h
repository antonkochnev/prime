#pragma once

#include <memory>
#include <string>

#include "grammar/Grammar.h"

namespace prime
{
    class TPTPSyntaxBNFParser
    {
    public:
        std::shared_ptr<Grammar> parse(const std::string& filename);
    };
}
