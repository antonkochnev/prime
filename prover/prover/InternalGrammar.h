#pragma once

#include <map>

#include "grammar/ParsingExpressionGrammar.h"

#include "FormulaConverter.h"
#include "Variable.h"

namespace prime
{
    class InternalGrammar
    {
    public:
        InternalGrammar();
        const ParsingExpressionGrammar& grammar() const;
        Sequent sequent(const std::string& str) const;
        FormulaPtr formula(const char* str) const;
        static std::string print(const Formula& fl);
        static std::string print(const Sequent& sequent);

    public:
        class FormulaConverter : public prime::FormulaConverter
        {
        public:
            virtual FormulaPtr convert(const Nonterminal& nt) override;
            virtual NonterminalPtr convert(const Formula& fl) override;

        private:
            const std::shared_ptr<Variable>& build_variable_(const std::string& name);
            FormulaPtr build_formula_(const Nonterminal& nt);

        private:
            std::map<std::string, std::shared_ptr<Variable>> variables_;
        };

        class SequentConverter : public prime::SequentConverter
        {
        public:
            virtual Sequent convert(const Nonterminal& nt) override;
            virtual NonterminalPtr convert(const Sequent& sequent) override;

        private:
            void build_formula_list_(const Nonterminal& nt, std::vector<FormulaPtr>& formula_list);
        };

    private:
        ParsingExpressionGrammar grammar_;
        ParsingExpression* formulaExpr_ = nullptr;
        ParsingExpression* sequentExpr_ = nullptr;
    };
}
