#pragma once

#include "grammar/Nonterminal.h"

#include "Formula.h"
#include "Sequent.h"

namespace prime
{
    class FormulaConverter
    {
    public:
        virtual ~FormulaConverter() = default;
        virtual FormulaPtr convert(const Nonterminal& nt) = 0;
        virtual NonterminalPtr convert(const Formula& fl) = 0;
    };

    class SequentConverter
    {
    public:
        virtual ~SequentConverter() = default;
        virtual Sequent convert(const Nonterminal& nt) = 0;
        virtual NonterminalPtr convert(const Sequent& sequent) = 0;
    };
}
