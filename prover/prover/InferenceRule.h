#pragma once

#include <memory>
#include <string>

#include "Sequent.h"

namespace prime
{
    class InferenceRule
    {
    public:
        virtual ~InferenceRule() = default;
        virtual std::string description() const = 0;
        virtual Sequent apply(const Sequent& src) = 0;
    };

    typedef std::unique_ptr<InferenceRule> InferenceRulePtr;
}
