#pragma once

#include <memory>

#include "FormulaVisitor.h"

namespace prime
{
    class Formula
    {
    public:
        virtual ~Formula() = default;
        virtual std::shared_ptr<Formula> clone() const = 0;
        virtual void visit(FormulaVisitor& visitor) const = 0;
    };

    typedef std::shared_ptr<Formula> FormulaPtr;
}
