#pragma once

#include "InferenceRule.h"

namespace prime
{
    class ImplicationInConjectureRule : public InferenceRule
    {
    public:
        ImplicationInConjectureRule(std::size_t inx);
        virtual std::string description() const override;
        virtual Sequent apply(const Sequent& src) override;

    private:
        std::size_t inx_;
    };
}
