#pragma once

#include <memory>
#include <vector>

#include "Formula.h"
#include "Sequent.h"

namespace prime
{
    class FormulaNormalizer
    {
    public:
        virtual ~FormulaNormalizer() = default;
        virtual FormulaPtr normalize(const FormulaPtr& formula) = 0;
        virtual Sequent normalize(const Sequent& sequent) = 0;
    };

    typedef std::shared_ptr<FormulaNormalizer> FormulaNormalizerPtr;

    class CompositeFormulaNormalizer : public FormulaNormalizer
    {
    public:
        void add(const FormulaNormalizerPtr& normalizer)
        {
            normalizers_.emplace_back(normalizer);
        }

        virtual FormulaPtr normalize(const FormulaPtr& formula) override
        {
            FormulaPtr res = formula;

            for (auto& normalizer : normalizers_)
            {
                res = normalizer->normalize(res);
            }

            return res;
        }

        virtual Sequent normalize(const Sequent& sequent) override
        {
            Sequent res = sequent;

            for (auto& normalizer : normalizers_)
            {
                res = normalizer->normalize(sequent);
            }

            return res;
        }

    private:
        std::vector<FormulaNormalizerPtr> normalizers_;
    };
}
