#pragma once

#include <set>

#include "grammar/Nonterminal.h"
#include "Sequent.h"
#include "Formula.h"

namespace prime
{
    class TPTPInput
    {
    public:
        Sequent sequent(const std::string& path) const;

    public:
        struct AnnotatedFormula
        {
            std::string name;
            std::string formula_role;
            NonterminalPtr formula;
        };

    public:
        std::vector<AnnotatedFormula> formulas;
        std::set<std::string> includes;
    };
}
