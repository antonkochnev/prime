#pragma once

#include "Formula.h"

namespace prime
{
    class ImplicationFormula : public Formula
    {
    public:
        ImplicationFormula(const FormulaPtr& arg1, const FormulaPtr& arg2);
        virtual FormulaPtr clone() const override;
        virtual void visit(FormulaVisitor& visitor) const override;
        const FormulaPtr& arg1() const;
        const FormulaPtr& arg2() const;

    private:
        FormulaPtr arg1_;
        FormulaPtr arg2_;
    };
}
