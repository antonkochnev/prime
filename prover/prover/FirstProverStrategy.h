#pragma once

#include "ProverStrategy.h"

namespace prime
{
    class FirstProverStrategy : public ProverStrategy
    {
    public:
        virtual void apply(ProofNode& node) override;

    private:
        static bool isAxiom(const Sequent& sequent);
    };
}
