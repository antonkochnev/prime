#pragma once

#include "FormulaVisitor.h"

namespace prime
{
    class FormulaWalker : public FormulaVisitor
    {
    public:
        virtual void handle(const NotFormula& fl) override;
        virtual void handle(const Variable& fl) override;
        virtual void handle(const ConjunctionFormula& fl) override;
        virtual void handle(const DisjunctionFormula& fl) override;
        virtual void handle(const ImplicationFormula& fl) override;

    protected:
        virtual void nodeNotFormula(const NotFormula& fl) {}
        virtual void nodeVariable(const Variable& fl) {};
        virtual void nodeConjunctionFormula(const ConjunctionFormula& fl) {};
        virtual void nodeDisjunctionFormula(const DisjunctionFormula& fl) {};
        virtual void nodeImplicationFormula(const ImplicationFormula& fl) {};
    };
}
