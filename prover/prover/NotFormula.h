#pragma once

#include "Formula.h"

namespace prime
{
    class NotFormula : public Formula
    {
    public:
        NotFormula(const FormulaPtr& arg);
        virtual FormulaPtr clone() const override;
        virtual void visit(FormulaVisitor& visitor) const override;
        const FormulaPtr& arg() const;

    private:
        FormulaPtr arg_;
    };
}
