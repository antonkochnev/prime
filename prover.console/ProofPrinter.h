#pragma once

#include <ostream>

#include "prover/Proof.h"

class ProofPrinter
{
public:
    void print(const prime::Proof& proof, std::ostream& os);
};
