#include <iostream>
#include <fstream>
#include <sstream>
#include <streambuf>
#include <string>

#include "prover/FirstProverStrategy.h"
#include "prover/Prover.h"
#include "prover/InternalGrammar.h"
#include "prover/TPTPGrammar.h"

#include "ProofPrinter.h"

using namespace prime;

void print_help()
{
    std::cout << "Usage:\n"
        << "prover.console <input-file>"
        << std::endl;
}

struct Options
{
    std::string src;
    std::string format;
    std::string includes;

    void print(std::ostream& os) const
    {
        os  << "src = " << src << '\n'
            << "format = " << format << '\n';
    }
};

std::string gev_option_value(int argc, char* argv[], int i)
{
    if (i + 1 < argc)
    {
        return argv[i + 1];
    }

    std::ostringstream oss;
    oss << "missed value for option '" << argv[i] << "'";
    throw std::runtime_error(oss.str());
}

Options parse_options(int argc, char* argv[])
{
    Options options;
    int i = 1;

    while (i < argc)
    {
        std::string arg = argv[i];
        std::transform(arg.begin(), arg.end(), arg.begin(), ::tolower);

        if (arg[0] == '-')
        {
            if (arg == "-format")
            {
                options.format = gev_option_value(argc, argv, i);
                ++i;
            }
            else if (arg == "-includes")
            {
                options.includes = gev_option_value(argc, argv, i);
                ++i;
            }
        }
        else
        {
            options.src = arg;
        }

        ++i;
    }

    return options;
}

int main(int argc, char* argv[])
{
    try
    {
        if (argc == 1)
        {
            print_help();
            return 0;
        }

        const auto options = parse_options(argc, argv);
        options.print(std::cout);

        Sequent sequent;

        if (options.format == "tptp")
        {
            TPTPGrammar grammar;
            const auto input = grammar.input_from_file(options.src);
            std::cout << "Formulas:\n";

            for (const auto& fl : input.formulas)
            {
                std::cout << fl.name << " " << fl.formula_role << "\n";
            }

            std::cout << "includes:\n";

            for (const auto& incl : input.includes)
            {
                std::cout << incl << "\n";
            }

            sequent = input.sequent(options.includes);
        }
        else
        {
            InternalGrammar grammar;
            std::ifstream t(options.src);
            const auto str = std::string((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
            sequent = grammar.sequent(str.c_str());
        }

        FirstProverStrategy strategy;
        Prover prover(strategy);
        const auto proof = prover.prove(sequent);
        std::cout << "Proof finished at " << proof->stats.elapsed << " ms" << std::endl;

        if (proof)
        {
            ProofPrinter printer;
            printer.print(*proof, std::cout);
        }
        else
        {
            std::cout << "Proof is null" << std::endl;
        }
    }
    catch (const std::exception& ex)
    {
        std::cerr << "ERROR: " << ex.what() << std::endl;
    }

    return 0;
}
