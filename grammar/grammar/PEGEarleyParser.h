#pragma once

#include <deque>
#include <list>
#include <memory>
#include <ostream>
#include <string>
#include <stack>
#include <vector>

#include "Nonterminal.h"
#include "ParsingExpression.h"

namespace prime
{
    class PEGEarleyParser
    {
    public:
        class CommentComsumer
        {
        public:
            virtual ~CommentComsumer() = default;
            virtual bool consume(char ch) = 0;
        };

        struct Stats
        {
            std::size_t situations_count = 0;
        };

    public:
        PEGEarleyParser(const ParsingExpression& start_symbol)
        :
            start_symbol_(start_symbol)
        {}

        NonterminalPtr parse(const std::string& source)
        {
            return parse_impl_(prepareSource_(source));
        }

        void set_spaces(const std::string& spaces)
        {
            spaces_ = spaces;
        }

        void set_comment_consumer(const std::shared_ptr<CommentComsumer>& comment_consumer)
        {
            comment_consumer_ = comment_consumer;
        }

        const Stats& stats() const
        {
            return stats_;
        }

        void print_states(const std::string& source, std::ostream& os) const
        {
            const std::vector<char> terminals = prepareSource_(source);

            for (std::size_t si = 0; si < states_.size(); ++si)
            {
                const State& state = states_[si];
                
                if (si == 0)
                {
                    os << "ZERO STATE:\n";
                }
                else
                {
                    os << "S" << si << ": '" << terminals[si - 1] << "'\n";
                }

                for (std::size_t i = 0; i < state.situations.size(); ++i)
                {
                    const Situation& s = state.situations[i];
                    os << i << ". [" << s.expr->name << "->";

                    if (s.expr->type == ParsingExpression::TerminalClass)
                    {
                        if (s.rule_pos == 0)
                        {
                            os << "*";
                        }

                        os << '[' << s.expr->terminals << ']';

                        if (s.rule_pos > 0)
                        {
                            os << "*";
                        }
                    }
                    else if (s.expr->type == ParsingExpression::TerminalSequence)
                    {
                        for (std::size_t ei = 0; ei < s.expr->terminals.size(); ++ei)
                        {
                            if (ei == s.rule_pos)
                            {
                                os << "*";
                            }

                            if (s.expr->terminals[ei])
                            {
                                os << s.expr->terminals[ei] << ' ';
                            }
                        }

                        if (s.rule_pos == s.expr->terminals.size())
                        {
                            os << "*";
                        }
                    }
                    else if (s.expr->type == ParsingExpression::Sequence)
                    {
                        for (std::size_t ei = 0; ei < s.expr->args.size(); ++ei)
                        {
                            if (ei == s.rule_pos)
                            {
                                os << "*";
                            }

                            if (s.expr->args[ei])
                            {
                                if (s.expr->args[ei]->type == ParsingExpression::TerminalSequence)
                                {
                                    os << s.expr->args[ei]->terminals << ' ';
                                }
                                else
                                {
                                    os << s.expr->args[ei]->name << ' ';
                                }
                            }
                        }

                        if (s.rule_pos == s.expr->args.size())
                        {
                            os << "*";
                        }
                    }
                    else if (s.expr->type == ParsingExpression::OrderedChoice)
                    {
                        os << '(';

                        for (std::size_t j = 0; j < s.expr->args.size(); ++j)
                        {
                            if (j)
                            {
                                os << '|';
                            }

                            if (s.rule_pos == j)
                            {
                                os << '*';
                            }

                            os << s.expr->args[j]->name;
                        }
                    }
                    else if (s.expr->type == ParsingExpression::OneOrMore)
                    {
                        if (s.rule_pos == 0)
                        {
                            os << "**" << s.expr->args[0]->name;
                        }
                        else
                        {
                            os << "*" << s.expr->args[0]->name << "*";
                        }
                    }

                    os << "," << s.str_pos << "]\n";
                }
            }
        }

    private:
        struct Situation
        {
            Situation(const ParsingExpression* e, std::size_t r_pos, std::size_t s_pos)
            :
                expr(e),
                rule_pos(r_pos),
                str_pos(s_pos)
            {}

            bool operator== (const Situation& arg) const
            {
                return (expr == arg.expr &&
                    rule_pos == arg.rule_pos &&
                    str_pos == arg.str_pos &&
                    left == arg.left);
            }

            bool is_prod_end() const
            {
                if (expr->type == ParsingExpression::TerminalSequence)
                {
                    return (expr->terminals.size() == rule_pos);
                }
                else if (expr->type == ParsingExpression::Sequence)
                {
                    return (expr->args.size() == rule_pos);
                }

                return (rule_pos > 0);
            }

            const ParsingExpression* expr;
            std::size_t rule_pos;
            std::size_t str_pos;
            const Situation* left = nullptr;
            const Situation* successor = nullptr;
        };

        struct State
        {
            std::deque<Situation> situations;

            Situation* find(const Situation& s)
            {
                for (Situation& i : situations)
                {
                    if (i == s)
                    {
                        return &i;
                    }
                }

                return nullptr;
            }
        };

        typedef std::vector<State> States;

    private:
        const ParsingExpression& start_symbol_;
        States states_;
        std::list<const Situation*> new_situations_;
        std::string spaces_;
        std::shared_ptr<CommentComsumer> comment_consumer_;
        bool token_to_terminals_ = true;
        Stats stats_;

    private:
        std::vector<char> prepareSource_(const std::string& source) const
        {
            auto bit = source.begin();

            while (bit != source.end() && is_space_(*bit))
            {
                ++bit;
            }

            if (bit == source.end())
            {
                return std::vector<char>();
            }

            auto eit = source.end();

            do
            {
                --eit;
            } while (eit != bit && is_space_(*eit));

            std::vector<char> terminals;
            terminals.reserve(source.length());

            for (auto it = bit; it != eit + 1; ++it)
            {
                if (!comment_consumer_ || !comment_consumer_->consume(*it))
                {
                    terminals.push_back(*it);
                }
            }

            return terminals;
        }

        NonterminalPtr parse_impl_(const std::vector<char>& source)
        {
            if (source.empty())
            {
                return nullptr;
            }

            stats_ = Stats();
            new_situations_.clear();
            states_.clear();
            states_.resize(source.size() + 1);
            make_zero_state_();
            int pre_space_pos = -1;
            std::size_t prev_index = 0;

            while (!new_situations_.empty())
            {
                predictor_(0, *new_situations_.front());
                completer_(states_[0], *new_situations_.front(), 0);
                new_situations_.pop_front();
            }

            for (std::size_t i = 0; i < source.size(); ++i)
            {
                if (is_space_(source[i]))
                {
                    pre_space_pos = i - 1;
                    while (++i < source.size() && is_space_(source[i]));
                }

                scanner_(source[i], i + 1, prev_index);
                prev_index = i + 1;

                if (new_situations_.empty())
                {
                    return nullptr;
                }

                while (!new_situations_.empty())
                {
                    predictor_(i + 1, *new_situations_.front());
                    completer_(states_[i + 1], *new_situations_.front(), pre_space_pos);
                    new_situations_.pop_front();
                }
            }

            return build_tree_(source);
        }

        void make_zero_state_()
        {
            auto& situations = states_[0].situations;
            situations.emplace_back(&start_symbol_, 0, 0);
            ++stats_.situations_count;
            new_situations_.push_back(&situations.back());
        }

        bool is_space_(char t) const
        {
            return (spaces_.find(t) != std::string::npos);
        }

        void scanner_(char terminal, std::size_t state_index, std::size_t state_prev_index)
        {
            State& state = states_[state_index];

            for (Situation& i : states_[state_prev_index].situations)
            {
                if (!i.is_prod_end())
                {
                    if (i.expr->type == ParsingExpression::TerminalClass)
                    {
                        if (i.expr->terminals.find(terminal) != std::string::npos)
                        {
                            state.situations.emplace_back(i.expr, i.rule_pos + 1, i.str_pos);
                            ++stats_.situations_count;
                            i.str_pos = state_index - 1;
                            Situation& s = state.situations.back();
                            s.left = &i;
                            new_situations_.push_back(&s);
                        }
                    }
                    else if (i.expr->type == ParsingExpression::TerminalSequence)
                    {
                        if (i.expr->terminals[i.rule_pos] == terminal)
                        {
                            state.situations.emplace_back(i.expr, i.rule_pos + 1, i.str_pos);
                            ++stats_.situations_count;
                            Situation& s = state.situations.back();
                            s.left = &i;
                            new_situations_.push_back(&s);
                        }
                    }
                }
            }
        }

        void predictor_(std::size_t state_index, const Situation& situation)
        {
            if (situation.expr->type == ParsingExpression::Sequence)
            {
                if (situation.rule_pos < situation.expr->args.size())
                {
                    const auto e = situation.expr->args[situation.rule_pos];
                    State& state = states_[state_index];
                    const Situation s(e, 0, state_index);

                    if (!state.find(s))
                    {
                        state.situations.emplace_back(s);
                        ++stats_.situations_count;
                        new_situations_.push_back(&state.situations.back());
                    }
                }
            }
            else if (situation.expr->type == ParsingExpression::OrderedChoice)
            {
                if (situation.rule_pos == 0)
                {
                    for (const auto e : situation.expr->args)
                    {
                        State& state = states_[state_index];
                        const Situation s(e, 0, state_index);

                        if (!state.find(s))
                        {
                            state.situations.emplace_back(s);
                            ++stats_.situations_count;
                            new_situations_.push_back(&state.situations.back());
                        }
                    }
                }
            }
            else if (situation.expr->type == ParsingExpression::OneOrMore)
            {
                const auto e = situation.expr->args[0];
                State& state = states_[state_index];
                const Situation s(e, 0, state_index);

                if (!state.find(s))
                {
                    state.situations.emplace_back(s);
                    ++stats_.situations_count;
                    new_situations_.push_back(&state.situations.back());
                }
            }
        }

        void completer_situation_(State& state, const Situation& situation, const Situation& i, ParsingExpression* e)
        {
            if (e->index == situation.expr->index)
            {
                Situation s(i.expr, i.rule_pos + 1, i.str_pos);
                s.left = &i;
                Situation* same = state.find(s);

                if (!same)
                {
                    state.situations.emplace_back(s);
                    ++stats_.situations_count;
                    Situation* ps = &state.situations.back();
                    new_situations_.push_back(ps);
                    same = ps;
                }

                if (!same->successor || (same->successor->expr->index > situation.expr->index))
                {
                    same->successor = &situation;
                }
            }
        }

        void completer_(State& state, const Situation& situation, int pre_space_pos)
        {
            if (situation.is_prod_end() && (situation.expr->allow_spaces || pre_space_pos <= static_cast<int>(situation.str_pos)))
            {
                const State& statej = states_[situation.str_pos];

                if (&statej == &state)
                {
                    throw std::runtime_error("&statej == &state");
                }

                for (const Situation& i : statej.situations)
                {
                    const bool is_prod_end = (i.expr->type == ParsingExpression::OneOrMore ? false : i.is_prod_end());

                    if (!is_prod_end)
                    {
                        if (i.expr->type == ParsingExpression::OrderedChoice)
                        {
                            for (const auto& e : i.expr->args)
                            {
                                completer_situation_(state, situation, i, e);
                            }
                        }
                        else if (i.expr->type != ParsingExpression::TerminalClass &&
                            i.expr->type != ParsingExpression::TerminalSequence)
                        {
                            const auto e = i.expr->args[i.expr->type == ParsingExpression::OneOrMore ? 0 : i.rule_pos];
                            completer_situation_(state, situation, i, e);
                        }
                    }
                }
            }
        }

        NonterminalPtr build_tree_(const std::vector<char>& source)
        {
            const Situation* s = nullptr;

            for (Situation& i : states_.back().situations)
            {
                if (i.str_pos == 0 &&
                    i.expr == &start_symbol_ &&
                    i.is_prod_end() &&
                    (!s || i.expr->index > s->expr->index))
                {
                    s = &i;
                }
            }

            NonterminalPtr res;

            if (s)
            {
                res = std::make_shared<Nonterminal>(s->expr->name);
                build_tree_level_(s, *res, source);
            }

            return res;
        }

        void build_tree_level_(const Situation* s, Nonterminal& node, const std::vector<char>& source)
        {
            std::stack<const Situation*> rhs;

            while (s)
            {
                rhs.push(s);
                s = s->left;
            }

            while (!rhs.empty())
            {
                const Situation* el = rhs.top();
                rhs.pop();

                if (!el->rule_pos)
                {
                    continue;
                }

                if (el->expr->type == ParsingExpression::TerminalClass)
                {
                    const char ch = source[el->left->str_pos];

                    if (token_to_terminals_)
                    {
                        node.join(ch);
                    }
                    else
                    {
                        node.add(ch);
                    }
                }
                else if (el->expr->type == ParsingExpression::TerminalSequence)
                {
                    const char ch = el->expr->terminals[el->rule_pos - 1];

                    if (token_to_terminals_)
                    {
                        node.join(ch);
                    }
                    else
                    {
                        node.add(ch);
                    }
                }
                else
                {
                    if (el->successor->expr->name.empty() || !el->successor->expr->allow_spaces)
                    {
                        build_tree_level_(el->successor, node, source);
                    }
                    else
                    {
                        NonterminalPtr next_node = std::make_shared<Nonterminal>(el->successor->expr->name);
                        build_tree_level_(el->successor, *next_node, source);
                        node.add(next_node);
                    }
                }
            }
        }
    };
}
