#pragma once

#include <string>
#include <vector>

namespace prime
{
    class Production
    {
    public:
        class Element
        {
        public:
            Element(const char terminal)
                : terminal_(terminal)
            {}

            Element(const std::string& nonterminal)
                : nonterminal_(nonterminal)
            {}

            char as_terminal() const
            {
                return terminal_;
            }

            const std::string as_nonterminal() const
            {
                return nonterminal_;
            }

            bool is_terminal() const
            {
                return nonterminal_.empty();
            }

            bool is_nonterminal() const
            {
                return !is_terminal();
            }

        private:
            char terminal_;
            std::string nonterminal_;
        };

        typedef std::vector<Element> Elements;

    public:
        Production(const std::string& name, std::size_t priority, bool allow_spaces)
        :
            name_(name),
            priority_(priority),
            allow_spaces_(allow_spaces)
        {}

        const std::string& name() const
        {
            return name_;
        }

        std::size_t priority() const
        {
            return priority_;
        }

        const Elements& elements() const
        {
            return elements_;
        }

        std::size_t size() const
        {
            return elements_.size();
        }

        bool allow_spaces() const
        {
            return allow_spaces_;
        }

        void add_element(char termianl)
        {
            elements_.emplace_back(termianl);
        }

        void add_element(const std::string& nonterminal)
        {
            elements_.emplace_back(nonterminal);
        }

    protected:
        std::string name_;
        std::size_t priority_ = 0;
        bool allow_spaces_;
        Elements elements_;
    };
}
