#pragma once

#include "CharacterClass.h"

namespace prime
{
    class Token
    :
        public ProductionBuilder
    {
    public:
        Token(Grammar& grammar, const std::string& name)
        :
            ProductionBuilder(grammar, name, false)
        {}

        Token& operator<< (char ch)
        {
            new_production();
            production_->add_element(ch);
            return *this;
        }

        Token& operator<< (const CharacterClass& pb)
        {
            new_production();
            production_->add_element(pb.getName());
            return *this;
        }

        Token& operator<< (const Token& pb)
        {
            new_production();
            production_->add_element(pb.getName());
            return *this;
        }

        Token& operator& (char ch)
        {
            production_->add_element(ch);
            return *this;
        }

        Token& operator& (const CharacterClass& pb)
        {
            production_->add_element(pb.getName());
            return *this;
        }

        Token& operator& (const Token& pb)
        {
            production_->add_element(pb.getName());
            return *this;
        }

        Token& operator| (char ch)
        {
            new_production();
            production_->add_element(ch);
            return *this;
        }

        Token& operator| (const CharacterClass& pb)
        {
            new_production();
            production_->add_element(pb.getName());
            return *this;
        }

        Token& operator| (const Token& pb)
        {
            new_production();
            production_->add_element(pb.getName());
            return *this;
        }
    };
}
