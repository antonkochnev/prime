#pragma once

#include "Nonterminal.h"

#include <map>
#include <sstream>
#include <string>

namespace prime
{
    inline void print(const Nonterminal& nt, std::ostream& os)
    {
        struct Visitor
        {
            Visitor(std::ostream& os)
                : os_(os)
            {}

            void operator()(const std::string& str)
            {
                os_ << str;
            }

            void operator()(const Nonterminal::Ptr& ptr)
            {
                print(*ptr, os_);
            }

            std::ostream& os_;
        };

        for (const auto& s : nt.symbols())
        {
            Visitor visitor(os);
            s.visit(visitor);
        }
    }

    inline std::string print(const Nonterminal& nt)
    {
        std::ostringstream oss;
        print(nt, oss);
        return oss.str();
    }

    inline void toDOT(const Nonterminal& nt, std::ostream& os, const char* name = nullptr, std::size_t* num = nullptr, std::map<std::string, std::string>* labels = nullptr)
    {
        if (!name)
        {
            os << "digraph {\n";

            if (nt[0].is_terminal())
            {
                os << "\"" << nt[0].terminals() << "\";\n";
            }
            else
            {
                const auto& ntname = nt.name();

                if (ntname.empty())
                {
                    throw std::runtime_error("Nonterminal with empty name");
                }

                std::size_t num_var = 0;
                std::map<std::string, std::string> labels_var;
                toDOT(nt, os, ntname.c_str(), &num_var, &labels_var);

                for (const auto& it : labels_var)
                {
                    os << it.first << "[label=\"" << it.second << "\"];\n";
                }
            }

            os << "}";
        }
        else
        {
            const auto node_num = *num;
            {
                std::ostringstream oss;
                oss << "node" << node_num;
                labels->emplace(oss.str(), name);
            }

            for (const auto& s : nt.symbols())
            {
                if (s.is_terminal())
                {
                    std::ostringstream oss;
                    oss << "node" << (++*num);
                    labels->emplace(oss.str(), "\\\"" + s.terminals() + "\\\"");
                    os << "node" << node_num << " -> node" << *num << ";\n";
                }
                else
                {
                    const auto& sname = s.nonterminal()->name();

                    if (sname.empty())
                    {
                        throw std::runtime_error("Nonterminal with empty name");
                    }

                    std::ostringstream oss;
                    oss << "node" << (++*num);
                    labels->emplace(oss.str(), sname);

                    os << "node" << node_num << " -> node" << *num << ";\n";
                    toDOT(*s.nonterminal(), os, sname.c_str(), num, labels);
                }
            }
        }
    }
}
