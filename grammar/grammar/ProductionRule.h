#pragma once

#include "Token.h"

namespace prime
{
    class ProductionRule
    :
        public ProductionBuilder
    {
    public:
        ProductionRule(Grammar& grammar, const std::string& name)
        :
            ProductionBuilder(grammar, name, true)
        {}

        ProductionRule& operator<< (char ch)
        {
            new_production();
            production_->add_element(ch);
            return *this;
        }

        ProductionRule& operator<< (const char* str)
        {
            new_production();

            while (*str)
            {
                production_->add_element(*str);
                ++str;
            }

            return *this;
        }

        ProductionRule& operator| (const char* str)
        {
            new_production();

            while (*str)
            {
                production_->add_element(*str);
                ++str;
            }

            return *this;
        }

        ProductionRule& operator<< (const ProductionBuilder& pb)
        {
            new_production();
            production_->add_element(pb.getName());
            return *this;
        }

        ProductionRule& operator& (char ch)
        {
            production_->add_element(ch);
            return *this;
        }

        ProductionRule& operator& (const char* str)
        {
            while (*str)
            {
                production_->add_element(*str);
                ++str;
            }

            return *this;
        }

        ProductionRule& operator& (const ProductionBuilder& pb)
        {
            production_->add_element(pb.getName());
            return *this;
        }

        ProductionRule& operator| (char ch)
        {
            new_production();
            production_->add_element(ch);
            return *this;
        }

        ProductionRule& operator| (const ProductionBuilder& pb)
        {
            new_production();
            production_->add_element(pb.getName());
            return *this;
        }
    };
}
