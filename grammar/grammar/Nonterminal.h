#pragma once

#include <memory>
#include <string>
#include <vector>

#include "Nonterminal.h"
#include "Production.h"

namespace prime
{
    class Nonterminal
    {
    public:
        typedef std::shared_ptr<Nonterminal> Ptr;

        class Symbol
        {
            friend class Nonterminal;

        public:
            Symbol(const Symbol& arg) = default;
            Symbol(Symbol&& arg) = default;
            Symbol& operator= (Symbol&&) = default;

            Symbol(char terminal)
            {
                terminals_ = terminal;
            }

            Symbol(const std::string& terminal)
            {
                terminals_ = terminal;
            }

            Symbol(const Ptr& nonterminal) : nonterminal_(nonterminal)
            {}

            bool is_terminal() const
            {
                return !nonterminal_;
            }

            const std::string& terminals() const
            {
                return terminals_;
            }

            const Ptr& nonterminal() const
            {
                return nonterminal_;
            }

            template<typename T>
            void visit(T& visitor) const
            {
                if (is_terminal())
                {
                    visitor(terminals());
                }
                else
                {
                    visitor(nonterminal());
                }
            }

        private:
            std::string terminals_;
            Ptr nonterminal_;
        };

        typedef std::vector<Symbol> Symbols;

    public:
        Nonterminal(const std::string& name)
        :
            name_(&name)
        {}

        Nonterminal(const std::string* name = nullptr)
        :
            name_(name)
        {}

        const std::string& name() const
        {
            return *name_;
        }

        const Symbols& symbols() const
        {
            return symbols_;
        }

        void add(const Symbol& s)
        {
            symbols_.emplace_back(s);
        }

        void add(char t)
        {
            symbols_.emplace_back(t);
        }

        void add(const std::string& str)
        {
            symbols_.emplace_back(str);
        }

        void add(const Ptr& nt)
        {
            symbols_.emplace_back(nt);
        }

        void join(char t)
        {
            const std::string str{ t };
            join(str);
        }

        void join(const std::string& str)
        {
            if (!symbols_.empty() && symbols_.back().is_terminal())
            {
                symbols_.back().terminals_ += str;
            }
            else
            {
                add(str);
            }
        }

        void join_front(char t)
        {
            const std::string str{ t };
            join_front(str);
        }

        void join_front(const std::string& str)
        {
            if (!symbols_.empty() && symbols_.back().is_terminal())
            {
                symbols_.back().terminals_ = str + symbols_.back().terminals_;
            }
            else
            {
                add(str);
            }
        }

        std::size_t size() const
        {
            return symbols_.size();
        }

        const Symbol& operator[] (std::size_t inx) const
        {
            return symbols_[inx];
        }

        const Symbol& get(std::size_t inx) const
        {
            return symbols_[inx];
        }
        const std::string& terminals(std::size_t inx) const
        {
            return symbols_[inx].terminals();
        }

        const Ptr& nonterminal(std::size_t inx) const
        {
            return symbols_[inx].nonterminal();
        }

        bool is_terminal(std::size_t inx) const
        {
            return (inx < symbols_.size() && symbols_[inx].is_terminal());
        }

        bool is_nonterminal(std::size_t inx) const
        {
            return (inx < symbols_.size() && !symbols_[inx].is_terminal());
        }

        void reverse()
        {
            std::reverse(symbols_.begin(), symbols_.end());
        }

    private:
        const std::string* name_;
        Symbols symbols_;
    };

    typedef std::shared_ptr<Nonterminal> NonterminalPtr;
}
