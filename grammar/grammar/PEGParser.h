#pragma once

#include <iostream>
#include <queue>
#include <ostream>
#include <set>
#include <string>
#include <utility>

#include "Nonterminal.h"
#include "ParsingExpression.h"

namespace prime
{
    class PEGParser : public PEGParserBase
    {
    private:
        class Situation
        {
        public:
            const ParsingExpression* expr = nullptr;
            std::size_t rule_pos = 0;
            int str_pos = 0;
            std::vector<std::shared_ptr<Situation>> top;
            std::shared_ptr<Situation> left;
            std::shared_ptr<Situation> successor;

            Situation(const ParsingExpression* e, int pos)
                : expr(e), str_pos(pos)
            {}

            int compare(const Situation& arg) const
            {
                if (expr->index != arg.expr->index)
                {
                    return (expr->index - arg.expr->index);
                }

                if (str_pos != arg.str_pos)
                {
                    return (str_pos - arg.str_pos);
                }

                return (rule_pos - arg.rule_pos);
            }

            bool less(const Situation& arg) const
            {
                return (compare(arg) < 0);
            }

            bool is_completed() const
            {
                if (expr->type == ParsingExpression::TerminalSequence)
                {
                    return (expr->terminals.size() == rule_pos);
                }
                else if (expr->type == ParsingExpression::Sequence)
                {
                    return (expr->args.size() == rule_pos);
                }

                return (rule_pos > 0);
            }

            bool isOrderedChoice() const
            {
                return expr->type == ParsingExpression::OrderedChoice;
            }

            std::ostream& print(std::ostream& os)
            {
                os << this << " ";

                if (expr->type == ParsingExpression::TerminalSequence)
                {
                    os << "TerminalSequence ";

                    if (!expr->name.empty())
                    {
                        os << '(' << expr->name << ") : ";
                    }

                    os << expr->terminals;
                }
                else if (expr->type == ParsingExpression::TerminalClass)
                {
                    os << "TerminalClass ";

                    if (!expr->name.empty())
                    {
                        os << '(' << expr->name << ") : ";
                    }

                    os << expr->terminals;
                }
                else if (expr->type == ParsingExpression::Sequence)
                {
                    os << "Sequence ";

                    if (!expr->name.empty())
                    {
                        os << '(' << expr->name << ") : ";
                    }

                    for (std::size_t i = 0; i < expr->args.size(); ++i)
                    {
                        const auto& arg = expr->args[i];

                        if (i)
                        {
                            os << " ";
                        }

                        if (!arg->name.empty())
                        {
                            os << arg->name;
                        }
                        else if (arg->type == ParsingExpression::OneOrMore)
                        {
                            os << '*' << arg->args[0]->name;
                        }
                        else
                        {
                            os << arg->terminals;
                        }
                    }
                }
                else if (isOrderedChoice())
                {
                    os << "OrderedChoice ";

                    if (!expr->name.empty())
                    {
                        os << '(' << expr->name << ") : ";
                    }

                    for (std::size_t i = 0; i < expr->args.size(); ++i)
                    {
                        const auto& arg = expr->args[i];

                        if (i)
                        {
                            os << " | ";
                        }

                        if (!arg->name.empty())
                        {
                            os << arg->name;
                        }
                        else if (arg->type == ParsingExpression::OneOrMore)
                        {
                            os << '*' << arg->args[0]->name;
                        }
                        else
                        {
                            os << arg->terminals;
                        }
                    }
                }
                else if (expr->type == ParsingExpression::OneOrMore)
                {
                    os << "OneOrMore ";

                    if (!expr->name.empty())
                    {
                        os << '(' << expr->name << ") : ";
                    }

                    os << expr->args[0]->name;
                }

                os << " rule_pos=" << rule_pos << " str_pos=" << str_pos;
                os << " top=[";

                for (const auto& s : top)
                {
                    os << s.get() << ',';
                }

                os << "]";

                if (left)
                {
                    os << " left = " << left.get();
                }

                if (successor)
                {
                    os << " successor = " << successor.get();
                }

                return os;
            }
        };

        typedef std::shared_ptr<Situation> SituationPtr;

        struct SituationPtrLess
        {
            bool operator() (const SituationPtr& arg1, const SituationPtr& arg2) const
            {
                return arg1->less(*arg2);
            }
        };

        typedef std::set<SituationPtr, SituationPtrLess> DiscoveredSituations;

    public:
        PEGParser(const ParsingExpression& start_symbol)
        :
            start_symbol_(start_symbol)
        {}

        NonterminalPtr parse(const std::string& source) override
        {
            prepare_();
            int last = static_cast<int>(source.size());

            while (last > 0 && is_space_(source[static_cast<std::size_t>(last - 1)]))
            {
                --last;
            }

            for (str_pos_ = 0; str_pos_ < last; ++str_pos_)
            {
                if (!is_space_(source[static_cast<std::size_t>(str_pos_)]))
                {
                    break;
                }
            }

            while (str_pos_ < last)
            {
                const char ch = source[static_cast<std::size_t>(str_pos_)];

                if (is_space_(ch))
                {
                    pre_space_pos_ = str_pos_ - 1;
                    while (++str_pos_ < last && is_space_(source[static_cast<std::size_t>(str_pos_)]));
                    continue;
                }

                process_terminal_(ch);
                ++str_pos_;
            }

            return build_tree_(source);
        }

        void set_spaces(const std::string& spaces)
        {
            spaces_ = spaces;
        }

        void set_token_to_terminals(bool token_to_terminals)
        {
            token_to_terminals_ = token_to_terminals;
        }

        void print_states(const std::string& source, std::ostream& os)
        {
            prepare_();
            os << "\nDiscovered situations:\n";

            for (const auto& it : discovered_situations_)
            {
                os << '\t';
                it->print(os);
                os << '\n';
            }

            int last = static_cast<int>(source.size());

            while (last > 0 && is_space_(source[static_cast<std::size_t>(last - 1)]))
            {
                --last;
            }

            for (str_pos_ = 0; str_pos_ < last; ++str_pos_)
            {
                if (!is_space_(source[static_cast<std::size_t>(str_pos_)]))
                {
                    break;
                }
            }

            while (str_pos_ < last)
            {
                const char ch = source[static_cast<std::size_t>(str_pos_)];

                if (is_space_(ch))
                {
                    pre_space_pos_ = str_pos_ - 1;
                    while (++str_pos_ < last && is_space_(source[static_cast<std::size_t>(str_pos_)]));
                    continue;
                }

                os << "'" << source[str_pos_] << "'\nTerminal situations:\n";

                for (const auto& n : terminal_situations_)
                {
                    os << '\t';
                    n->print(os);
                    os << '\n';
                }

                process_terminal_(source[str_pos_]);
                ++str_pos_;
                os << "\nDiscovered situations:\n";

                for (const auto& it : discovered_situations_)
                {
                    os << '\t';
                    it->print(os);
                    os << '\n';
                }

                os << std::endl;
            }
        }

    private:
        void prepare_()
        {
            str_pos_ = -1;
            pre_space_pos_ = -1;
            discovered_situations_.clear();
            new_situations_ = std::queue<SituationPtr>();
            terminal_situations_.clear();
            auto root_node = std::make_shared<Situation>(&start_symbol_, str_pos_);
            add_new_situation_(root_node);
            process_new_nodes_();
        }

        void process_terminal_(char ch)
        {
            discovered_situations_.clear();
            std::vector<SituationPtr> terminal_situations;
            terminal_situations_.swap(terminal_situations);

            for (auto& n : terminal_situations)
            {
                scanner_(ch, n);
            }

            process_new_nodes_();
        }

        void process_new_nodes_()
        {
            while (!new_situations_.empty())
            {
                predictor_(new_situations_.front());
                completer_(new_situations_.front());
                new_situations_.pop();
            }
        }

        NonterminalPtr build_tree_(const std::string& source)
        {
            NonterminalPtr res;
            const Situation* root = nullptr;

            for (const auto& s : discovered_situations_)
            {
                if (s->str_pos == -1 && &start_symbol_ == s->expr)
                {
                    root = s.get();
                }
            }

            if (root && root->is_completed())
            {
                res = std::make_shared<Nonterminal>(&(root->expr->name));
                build_tree_level_(source, root, *res);
                res->reverse();
            }

            return res;
        }

        void build_tree_level_(const std::string& source, const Situation* s, Nonterminal& nt)
        {
            if (s->expr->type == ParsingExpression::TerminalSequence)
            {
                if (token_to_terminals_)
                {
                    nt.join_front(s->expr->terminals);
                }
                else
                {
                    nt.add(s->expr->terminals);
                }
            }
            else if (s->expr->type == ParsingExpression::TerminalClass)
            {
                const char ch = source[static_cast<std::size_t>(s->str_pos)];

                if (token_to_terminals_)
                {
                    nt.join_front(ch);
                }
                else
                {
                    nt.add(ch);
                }
            }
            else
            {
                while (s)
                {
                    if (s->successor)
                    {
                        if (s->successor->expr->name.empty() || !s->successor->expr->allow_spaces)
                        {
                            build_tree_level_(source, s->successor.get(), nt);
                        }
                        else
                        {
                            auto res = std::make_shared<Nonterminal>(&(s->successor->expr->name));
                            build_tree_level_(source, s->successor.get(), *res);
                            res->reverse();
                            nt.add(res);
                        }
                    }

                    s = s->left.get();
                }
            }
        }

        void scanner_(char next, SituationPtr& situation)
        {
            const auto& expr = *(situation->expr);

            if (expr.type == ParsingExpression::TerminalSequence)
            {
                if (expr.terminals[situation->rule_pos] == next)
                {
                    situation->rule_pos += 1;
                    add_new_situation_(situation);
                }
            }
            else if (expr.type == ParsingExpression::TerminalClass)
            {
                if (expr.terminals.find(next) != std::string::npos)
                {
                    situation->rule_pos += 1;
                    situation->str_pos = str_pos_;
                    add_new_situation_(situation);
                }
            }
        }

        void predictor_(const SituationPtr& situation)
        {
            const auto& expr = *(situation->expr);

            if (expr.type == ParsingExpression::OrderedChoice)
            {
                if (!situation->is_completed())
                {
                    for (const auto& arg : expr.args)
                    {
                        auto s = std::make_shared<Situation>(arg, str_pos_);
                        s->top.push_back(situation);
                        add_new_situation_(s);
                    }
                }
            }
            else if (expr.type == ParsingExpression::OneOrMore)
            {
                for (const auto& arg : expr.args)
                {
                    auto s = std::make_shared<Situation>(arg, str_pos_);
                    s->top.push_back(situation);
                    add_new_situation_(s);
                }
            }
            else if (expr.type == ParsingExpression::Sequence)
            {
                if (!situation->is_completed())
                {
                    const auto& arg = expr.args[situation->rule_pos];
                    auto s = std::make_shared<Situation>(arg, str_pos_);
                    s->top.push_back(situation);
                    add_new_situation_(s);
                }
            }
        }

        void add_new_situation_(const SituationPtr& situation)
        {
            auto p = discovered_situations_.insert(situation);

            if (p.second)
            {
                if (!situation->expr->terminals.empty() && !situation->is_completed())
                {
                    terminal_situations_.push_back(situation);
                }
                else
                {
                    new_situations_.push(situation);
                }
            }
            else
            {
                auto& s = **p.first;

                if (s.isOrderedChoice() && s.successor)
                {
                    for (const auto& arg : s.expr->args)
                    {
                        if (arg->index == s.successor->expr->index)
                        {
                            break;
                        }
                        else if (arg->index == situation->successor->expr->index)
                        {
                            s.successor = situation->successor;
                            break;
                        }
                    }
                }
                else if (situation->rule_pos == 1 && situation->expr->type == ParsingExpression::OneOrMore)
                {

                }
                else
                {
                    s.top.insert(s.top.end(), situation->top.begin(), situation->top.end());
                }
            }
        }

        void completer_(const SituationPtr& situation)
        {
            if (situation->is_completed() && space_check(*situation))
            {
                for (auto& s : situation->top)
                {
                    auto news = std::make_shared<Situation>(s->expr, s->str_pos);
                    news->rule_pos = (s->expr->type == ParsingExpression::OneOrMore ? 1 : s->rule_pos + 1);
                    news->successor = situation;
                    news->top = s->top;

                    if (s->rule_pos)
                    {
                        news->left = s;
                    }

                    add_new_situation_(news);
                }
            }
        }

        bool space_check(const Situation& situation) const
        {
            return (situation.expr->allow_spaces || pre_space_pos_ <= situation.str_pos);
        }

        bool is_space_(char t) const
        {
            return (spaces_.find(t) != std::string::npos);
        }

    private:
        const ParsingExpression& start_symbol_;
        std::string spaces_;
        bool token_to_terminals_ = true;

        DiscoveredSituations discovered_situations_;
        std::queue<SituationPtr> new_situations_;
        std::vector<SituationPtr> terminal_situations_;
        int str_pos_;
        int pre_space_pos_;
    };
}
