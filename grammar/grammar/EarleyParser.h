#pragma once

#include <deque>
#include <list>
#include <memory>
#include <ostream>
#include <string>
#include <stack>
#include <vector>

#include "Grammar.h"
#include "Nonterminal.h"
#include "Production.h"

namespace prime
{
    class EarleyParser
    {
    public:
        class CommentComsumer
        {
        public:
            virtual ~CommentComsumer() = default;
            virtual bool consume(char ch) = 0;
        };

    public:
        EarleyParser(const Grammar& grammar, const std::string& start_symbol)
        :
            grammar_(grammar),
            start_symbol_(start_symbol)
        {}

        NonterminalPtr parse(const std::string& source)
        {
            return parse_impl(prepareSource_(source));
        }

        void set_spaces(const std::string& spaces)
        {
            spaces_ = spaces;
        }

        void set_comment_consumer(const std::shared_ptr<CommentComsumer>& comment_consumer)
        {
            comment_consumer_ = comment_consumer;
        }

        void print_states(const std::string& source, std::ostream& os) const
        {
            const std::vector<char> terminals = prepareSource_(source);

            for (std::size_t si = 0; si < states_.size(); ++si)
            {
                const State& state = states_[si];
                os << "S" << si << ": '" << terminals[si] << "'\n";

                for (std::size_t i = 0; i < state.situations.size(); ++i)
                {
                    const Situation& s = state.situations[i];
                    os << i << ". [" << s.production->name() << "->";
                    const auto& elements = s.production->elements();

                    for (std::size_t ei = 0; ei < elements.size(); ++ei)
                    {
                        if (ei == s.prod_pos)
                        {
                            os << "*";
                        }

                        if (elements[ei].is_terminal())
                        {
                            os << elements[ei].as_terminal() << ' ';
                        }
                        else
                        {
                            os << elements[ei].as_nonterminal() << ' ';
                        }
                    }

                    if (s.prod_pos == elements.size())
                    {
                        os << "*";
                    }

                    os << "," << s.str_pos << "]\n";
                }
            }
        }

    private:
        struct Situation
        {
            Situation(const Production* p, std::size_t p_pos, std::size_t s_pos)
            :
                production(p),
                prod_pos(p_pos),
                str_pos(s_pos)
            {}

            bool operator== (const Situation& arg) const
            {
                return (production == arg.production &&
                    prod_pos == arg.prod_pos &&
                    str_pos == arg.str_pos &&
                    left == arg.left);
            }

            bool is_prod_end() const
            {
                return (prod_pos == production->size());
            }

            const typename Production::Element& prod_symbol() const
            {
                return production->elements()[prod_pos];
            }

            const Production* production;
            std::size_t prod_pos;
            std::size_t str_pos;
            const Situation* left = nullptr;
            const Situation* successor = nullptr;
        };

        struct State
        {
            std::deque<Situation> situations;

            Situation* find(const Situation& s)
            {
                for (Situation& i : situations)
                {
                    if (i == s)
                    {
                        return &i;
                    }
                }

                return nullptr;
            }
        };

        typedef std::vector<State> States;

    private:
        const Grammar& grammar_;
        const std::string start_symbol_;
        States states_;
        std::list<const Situation*> new_situations_;
        std::string spaces_;
        std::shared_ptr<CommentComsumer> comment_consumer_;

    private:
        std::vector<char> prepareSource_(const std::string& source) const
        {
            auto bit = source.begin();

            while (bit != source.end() && is_space(*bit))
            {
                ++bit;
            }

            if (bit == source.end())
            {
                return std::vector<char>();
            }

            auto eit = source.end();

            do
            {
                --eit;
            }
            while (eit != bit && is_space(*eit));

            std::vector<char> terminals;
            terminals.reserve(source.length());

            for (auto it = bit; it != eit + 1; ++it)
            {
                if (!comment_consumer_ || !comment_consumer_->consume(*it))
                {
                    terminals.push_back(*it);
                }
            }

            terminals.push_back(char());
            return terminals;
        }

        NonterminalPtr parse_impl(const std::vector<char>& source)
        {
            if (source.empty())
            {
                return nullptr;
            }

            new_situations_.clear();
            states_.clear();
            states_.resize(source.size());
            make_zero_state();

            std::size_t space_pos = 0;
            std::size_t prev_index = 0;

            for (std::size_t i = 0; i < source.size(); ++i)
            {
                if (is_space(source[i]))
                {
                    space_pos = i;
                    continue;
                }

                if (i)
                {
                    scanner(source[prev_index], i, prev_index, space_pos);
                }

                if (new_situations_.empty())
                {
                    return nullptr;
                }

                while (!new_situations_.empty())
                {
                    predictor(i, *new_situations_.front(), source[i]);
                    completer(states_[i], *new_situations_.front(), space_pos);
                    new_situations_.pop_front();
                }

                prev_index = i;
            }

            return build_tree();
        }

        void make_zero_state()
        {
            auto& situations = states_[0].situations;

            for (const Production& prod : grammar_.get_productions())
            {
                if (prod.name() == start_symbol_)
                {
                    situations.emplace_back(&prod, 0, 0);
                    new_situations_.push_back(&situations.back());
                }
            }
        }

        bool is_space(char t) const
        {
            return (spaces_.find(t) != std::string::npos);
        }

        void scanner(char terminal, std::size_t state_index, std::size_t state_prev_index, std::size_t space_pos)
        {
            State& state = states_[state_index];

            for (const Situation& i : states_[state_prev_index].situations)
            {
                if (!i.is_prod_end() &&
                    i.prod_symbol().is_terminal() &&
                    i.prod_symbol().as_terminal() == terminal)
                {
                    if ((space_pos <= state_prev_index) || i.production->allow_spaces() || i.prod_pos == 0 || (i.prod_pos == i.production->size() - 1))
                    {
                        state.situations.emplace_back(i.production, i.prod_pos + 1, i.str_pos);
                        Situation& s = state.situations.back();
                        s.left = &i;
                        new_situations_.push_back(&s);
                    }
                }
            }
        }

        void predictor(std::size_t state_index, const Situation& situation, char next)
        {
            if (!situation.is_prod_end() && !situation.prod_symbol().is_terminal())
            {
                for (const Production& prod : grammar_.get_productions())
                {
                    if (situation.prod_symbol().as_nonterminal() == prod.name() &&
                        (!prod.elements().front().is_terminal() || prod.elements().front().as_terminal() == next))
                    {
                        State& state = states_[state_index];
                        const Situation s(&prod, 0, state_index);

                        if (!state.find(s))
                        {
                            state.situations.emplace_back(s);
                            new_situations_.push_back(&state.situations.back());
                        }
                    }
                }
            }
        }

        void completer(State& state, const Situation& situation, std::size_t space_pos)
        {
            if (situation.is_prod_end())
            {
                const State& statej = states_[situation.str_pos];

                if (&statej == &state)
                {
                    throw std::runtime_error("&statej == &state");
                }

                for (const Situation& i : statej.situations)
                {
                    if (!i.is_prod_end() &&
                        !i.prod_symbol().is_terminal() &&
                        i.prod_symbol().as_nonterminal() == situation.production->name())
                    {
                        if (space_pos <= i.str_pos ||
                            i.production->allow_spaces() ||
                            i.prod_pos == i.production->size() - 1)
                        {
                            Situation s(i.production, i.prod_pos + 1, i.str_pos);
                            s.left = &i;
                            Situation* same = state.find(s);

                            if (!same)
                            {
                                state.situations.emplace_back(s);
                                Situation* ps = &state.situations.back();
                                new_situations_.push_back(ps);
                                same = ps;
                            }

                            if (!same->successor || (same->successor->production->priority() > situation.production->priority()))
                            {
                                same->successor = &situation;
                            }
                        }
                    }
                }
            }
        }

        NonterminalPtr build_tree()
        {
            const Situation* s = nullptr;

            for (Situation& i : states_.back().situations)
            {
                if (i.str_pos == 0 &&
                    i.production->name() == start_symbol_ &&
                    i.is_prod_end() &&
                    (!s || i.production->priority() > s->production->priority()))
                {
                    s = &i;
                }
            }

            NonterminalPtr res;

            if (s)
            {
                res = std::make_shared<Nonterminal>(s->production->name());
                build_tree_level(s, *res);
            }

            return res;
        }

        void build_tree_level(const Situation* s, Nonterminal& node)
        {
            std::stack<const Situation*> rhs;

            while (s)
            {
                rhs.push(s);
                s = s->left;
            }

            while (!rhs.empty())
            {
                const Situation* el = rhs.top();
                rhs.pop();

                if (!el->prod_pos)
                {
                    continue;
                }

                const typename Production::Element& symbol = el->production->elements()[el->prod_pos - 1];

                if (symbol.is_terminal())
                {
                    node.add(symbol.as_terminal());
                }
                else if (el->successor)
                {
                    NonterminalPtr next_node = std::make_shared<Nonterminal>(el->successor->production->name());
                    build_tree_level(el->successor, *next_node);
                    node.add(next_node);
                }
            }
        }
    };
}
