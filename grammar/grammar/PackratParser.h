#pragma once

#include <map>
#include <set>
#include <string>
#include <vector>

#include "Nonterminal.h"
#include "ParsingExpression.h"

//#define PACKRAT_PARSER_TRACE

#ifdef PACKRAT_PARSER_TRACE
#include <iostream>
#endif

namespace prime
{
    class PackratParser : public PEGParserBase
    {
    public:
        PackratParser(const ParsingExpression& start_symbol)
        :
            start_symbol_(start_symbol)
        {}

        NonterminalPtr parse(const std::string& source) override
        {
            memoized_.clear();
            memoized_.resize(source.length());

#ifdef PACKRAT_PARSER_TRACE
            std::cout << "memoized_.resize(" << source.length() << ")" << std::endl;
#endif
            if (comment_symbol_)
            {
                find_comments_(source);
            }

            NonterminalPtr res;
            MemoizedEntry entry;
            memoized_entry_(0, start_symbol_, source, entry);

            std::size_t last = static_cast<int>(source.size());

            while (last > 0 && is_space_(source[last - 1]))
            {
                --last;
            }

            if (entry.last_pos != NO_ITEM && entry.last_pos == last)
            {
                if (entry.terminals.empty())
                {
                    res = entry.nonterminal;
                }
                else
                {
                    res = std::make_shared<Nonterminal>(start_symbol_.name);
                    res->add(entry.terminals);
                }
            }

            return res;
        }

        void set_spaces(const std::string& spaces)
        {
            spaces_ = spaces;
        }

        void set_comment_symbol(const ParsingExpression* comment_symbol)
        {
            comment_symbol_ = comment_symbol;
        }

    private:
        static const std::size_t NO_ITEM = static_cast<std::size_t>(-1);
        static const std::size_t ON_STACK = static_cast<std::size_t>(-2);

        struct MemoizedEntry
        {
            std::string terminals;
            NonterminalPtr nonterminal;
            std::size_t last_pos = NO_ITEM;

            MemoizedEntry() = default;

            MemoizedEntry(std::size_t pos) : last_pos(pos)
            {}
        };

        struct MemoizedSlot : public std::map<int, MemoizedEntry>
        {
            std::size_t comment_length = 0;
        };

    private:
        const ParsingExpression& start_symbol_;
        std::string spaces_;
        std::vector<MemoizedSlot> memoized_;
        std::set<std::pair<std::size_t, int>> stack_; // str_pos + expr.index
        const ParsingExpression* comment_symbol_ = nullptr;

    private:
        void find_comments_(const std::string& source)
        {
#ifdef PACKRAT_PARSER_TRACE
            std::cout << "find_comments_" << std::endl;
#endif
            const auto tmp_spaces = spaces_;
            std::size_t str_pos = 0;

            while (str_pos < source.length())
            {
                MemoizedEntry entry;
                memoized_entry_(str_pos, *comment_symbol_, source, entry);

                if (entry.last_pos != NO_ITEM)
                {
                    auto& memoized_for_pos = memoized_[str_pos];
                    memoized_for_pos.comment_length = entry.last_pos - str_pos;
#ifdef PACKRAT_PARSER_TRACE
                    std::cout << "memoized_for_pos.comment_length = " << memoized_for_pos.comment_length << std::endl;
                    std::cout << "str_pos = " << str_pos << std::endl;
#endif
                    str_pos = entry.last_pos;
                }
                else
                {
                    ++str_pos;
                }
            }

            spaces_ = tmp_spaces;
        }

        void add(const ParsingExpression& expr, const MemoizedEntry& e, MemoizedEntry& entry)
        {
            if (!entry.nonterminal)
            {
                entry.nonterminal = std::make_shared<Nonterminal>(&expr.name);
            }

            if (e.terminals.empty())
            {
                if (e.nonterminal->name().empty())
                {
                    for (const auto& s : e.nonterminal->symbols())
                    {
                        entry.nonterminal->add(s);
                    }
                }
                else
                {
                    entry.nonterminal->add(e.nonterminal);
                }
            }
            else
            {
                if (expr.allow_spaces)
                {
                    entry.nonterminal->add(e.terminals);
                }
                else if (entry.nonterminal->size() == 0)
                {
                    entry.terminals += e.terminals;
                }
                else
                {
                    entry.nonterminal->join(e.terminals);
                }
            }
        }

        const MemoizedEntry& symbol_(std::size_t str_pos, const ParsingExpression& expr, const std::string& source)
        {
            if (str_pos >= source.size())
            {
                static const MemoizedEntry EMPTY_ENTRY;
                return EMPTY_ENTRY;
            }

            auto& memoized_for_pos = memoized_[str_pos];
            auto it = memoized_for_pos.find(expr.index);

            if (it == memoized_for_pos.end())
            {
                static const MemoizedEntry ON_STACK_ENTRY(ON_STACK);
                const auto pair = stack_.emplace(str_pos, expr.index);

                if (!pair.second)
                {
                    return ON_STACK_ENTRY;
                }

                MemoizedEntry entry;
                memoized_entry_(str_pos, expr, source, entry);
                stack_.erase(std::make_pair(str_pos, expr.index));

                if (entry.last_pos != ON_STACK)
                {
                    it = memoized_for_pos.emplace(expr.index, entry).first;
                }
                else
                {
                    return ON_STACK_ENTRY;
                }
            }

            return it->second;
        }

        void memoized_entry_(std::size_t str_pos, const ParsingExpression& expr, const std::string& source, MemoizedEntry& entry)
        {
#ifdef PACKRAT_PARSER_TRACE
            std::cout << "memoized_entry_(" << str_pos << ", " << expr.name << ", " << source[str_pos] << ")" << std::endl;
#endif
            str_pos = roll_comment_(str_pos);

            if (expr.type == ParsingExpression::TerminalClass)
            {
                terminal_class_(str_pos, expr, source, entry);
            }
            else if (expr.type == ParsingExpression::TerminalSequence)
            {
                terminal_sequence_(str_pos, expr, source, entry);
            }
            else if (expr.type == ParsingExpression::Sequence)
            {
                sequence_(str_pos, expr, source, entry);
            }
            else if (expr.type == ParsingExpression::OrderedChoice)
            {
                ordered_choice_(str_pos, expr, source, entry);
            }
            else if (expr.type == ParsingExpression::OneOrMore)
            {
                one_or_more_(str_pos, expr, source, entry);
            }
            else // null
            {
                null_(str_pos, expr, source, entry);
            }
        }

        void null_(std::size_t str_pos, const ParsingExpression& expr, const std::string&, MemoizedEntry& entry)
        {
            entry.last_pos = str_pos;
            entry.nonterminal = std::make_shared<Nonterminal>(&expr.name);
        }

        void sequence_(std::size_t str_pos, const ParsingExpression& expr, const std::string& source, MemoizedEntry& entry)
        {
            MemoizedEntry res;
            res.last_pos = 0;

            for (const auto& arg : expr.args)
            {
                str_pos = roll_spaces_(str_pos, expr, source);
                const auto& e = symbol_(str_pos, *arg, source);

                if (e.last_pos == NO_ITEM || e.last_pos == ON_STACK)
                {
                    entry = e.last_pos;
                    return;
                }

                res.last_pos = e.last_pos;
                str_pos = e.last_pos;
                add(expr, e, res);
            }

            entry = std::move(res);
        }

        void one_or_more_(std::size_t str_pos, const ParsingExpression& expr, const std::string& source, MemoizedEntry& entry)
        {
            MemoizedEntry res;
            res.last_pos = 0;

            while (str_pos < source.size())
            {
                str_pos = roll_spaces_(str_pos, expr, source);
                const auto& e = symbol_(str_pos, *(expr.args[0]), source);

                if (e.last_pos == NO_ITEM || e.last_pos == ON_STACK)
                {
                    entry = e.last_pos;
                    break;
                }

                res.last_pos = e.last_pos;
                str_pos = e.last_pos;
                add(expr, e, res);
            }

            if (res.last_pos > 0)
            {
                entry = std::move(res);
            }
        }

        void ordered_choice_(std::size_t str_pos, const ParsingExpression& expr, const std::string& source, MemoizedEntry& entry)
        {
            str_pos = roll_spaces_(str_pos, expr, source);

            for (const auto& arg : expr.args)
            {
                const auto& e = symbol_(str_pos, *arg, source);

                if (e.last_pos == ON_STACK)
                {
                    entry.last_pos = ON_STACK;
                }
                else if (e.last_pos != NO_ITEM)
                {
                    entry.last_pos = e.last_pos;
                    //entry.nonterminal = std::make_shared<Nonterminal>(&arg->name);
                    add(expr, e, entry);
                    return;
                }
            }
        }

        std::size_t roll_comment_(std::size_t str_pos)
        {
            while (memoized_[str_pos].comment_length && str_pos < memoized_.size())
            {
                str_pos += memoized_[str_pos].comment_length;
            }

            return str_pos;
        }

        void terminal_class_(std::size_t str_pos, const ParsingExpression& expr, const std::string& source, MemoizedEntry& entry)
        {
            if (str_pos < source.size() && expr.terminals.find(source[str_pos]) != std::string::npos)
            {
                entry.terminals += source[str_pos];
                entry.last_pos = str_pos + 1;
            }
        }

        void terminal_sequence_(std::size_t str_pos, const ParsingExpression& expr, const std::string& source, MemoizedEntry& entry)
        {
            if (source.size() - str_pos >= expr.terminals.size())
            {
                for (std::size_t pos = 0; pos < expr.terminals.size(); ++pos)
                {
                    const std::size_t i = str_pos + pos;

                    if (source[i] != expr.terminals[pos])
                    {
                        return;
                    }
                }

                if (expr.name.empty())
                {
                    entry.terminals = expr.terminals;
                }
                else
                {
                    entry.nonterminal = std::make_shared<Nonterminal>(&expr.name);
                    entry.nonterminal->add(expr.terminals);
                }


                entry.last_pos = str_pos + expr.terminals.size();
            }
        }

        bool is_space_(char t) const
        {
            return (spaces_.find(t) != std::string::npos);
        }

        std::size_t roll_spaces_(std::size_t str_pos, const ParsingExpression& expr, const std::string& source)
        {
            if (expr.allow_spaces)
            {
                while (str_pos < source.size())
                {
                    if (is_space_(source[str_pos]))
                    {
                        ++str_pos;
                    }
                    else if (memoized_[str_pos].comment_length)
                    {
                        str_pos += memoized_[str_pos].comment_length;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return str_pos;
        }
    };
}
