#pragma once

#include "ProductionBuilder.h"

namespace prime
{
    class CharacterClass
    :
        public ProductionBuilder
    {
    public:
        CharacterClass(Grammar& grammar, const std::string& name)
        :
            ProductionBuilder(grammar, name, false)
        {}

        CharacterClass& operator<< (const char* str)
        {
            for (const auto ch : std::string(str))
            {
                new_production();
                production_->add_element(ch);
            }

            return *this;
        }

        CharacterClass& operator| (const char* str)
        {
            for (const auto ch : std::string(str))
            {
                new_production();
                production_->add_element(ch);
            }

            return *this;
        }

        CharacterClass& operator<< (const CharacterClass& cc)
        {
            new_production();
            production_->add_element(cc.getName());
            return *this;
        }

        CharacterClass& operator| (const CharacterClass& cc)
        {
            new_production();
            production_->add_element(cc.getName());
            return *this;
        }
    };
}
