#pragma once

#include <memory>
#include <string>
#include <vector>

#include "Nonterminal.h"

namespace prime
{
    class ParsingExpression
    {
    public:
        enum Type
        {
            TerminalSequence,
            TerminalClass,
            Sequence,
            OrderedChoice,
            OneOrMore,
            Undefined
        };

    public:
        ParsingExpression(std::size_t inx)
            : index(inx)
        {}

        ParsingExpression(const ParsingExpression&) = delete;
        ParsingExpression& operator=(const ParsingExpression&) = delete;
        virtual ~ParsingExpression() = default;

        void copy(const ParsingExpression& arg)
        {
            type = arg.type;
            terminals = arg.terminals;
            args = arg.args;
            allow_spaces = arg.allow_spaces;
        }

        void swap(ParsingExpression& arg)
        {
            ParsingExpression tmp(0);
            tmp.copy(arg);
            arg.copy(*this);
            copy(tmp);
        }

    public:
        Type type = Undefined;
        std::string name;
        std::size_t index = 0;
        std::string terminals;
        std::vector<ParsingExpression*> args;
        bool allow_spaces = false;
    };

    class PEGParserBase
    {
    public:
        virtual ~PEGParserBase()
        {}

        virtual NonterminalPtr parse(const std::string& source) = 0;
    };
}
