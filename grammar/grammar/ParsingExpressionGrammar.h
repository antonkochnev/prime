#pragma once

#include <deque>
#include <string>
#include <vector>

#include "ParsingExpression.h"

namespace prime
{
    class ParsingExpressionGrammar
    {
    public:
        ParsingExpressionGrammar(const std::string& name)
        :
            name_(name)
        {}

        const std::string& name() const
        {
            return name_;
        }

        ParsingExpression& add_rule(const std::string& name)
        {
            std::size_t i = 0;

            for (; i < rules_.size(); ++i)
            {
                if (rules_[i].name == name)
                {
                    if (rules_[i].type == ParsingExpression::Undefined)
                    {
                        return rules_[i];
                    }

                    break;
                }
            }

            if (i < rules_.size())
            {
                auto& rule = make_rule();
                auto& curr = rules_[i];
                rule.allow_spaces = curr.allow_spaces;

                if (curr.type == ParsingExpression::OrderedChoice)
                {
                    curr.args.push_back(&rule);
                }
                else if (curr.type == ParsingExpression::Sequence && curr.args.size() == 1)
                {
                    curr.type = ParsingExpression::OrderedChoice;
                    curr.args.push_back(&rule);
                }
                else
                {
                    auto& tmp = make_rule();
                    tmp.copy(curr);

                    curr.type = ParsingExpression::OrderedChoice;
                    curr.terminals.clear();
                    curr.args.clear();
                    curr.args.push_back(&tmp);
                    curr.args.push_back(&rule);
                }

                return rule;
            }

            auto& rule = make_rule();
            rule.name = name;
            return rule;
        }

        ParsingExpression& make_rule()
        {
            const auto index = rules_.size();
            rules_.emplace_back(index);
            return rules_.back();
        }

        const ParsingExpression& get_rule(const std::string name) const
        {
            for (const auto& r : rules_)
            {
                if (r.name == name)
                {
                    return r;
                }
            }

            throw std::runtime_error("rule '" + name + "' not found");
        }

    private:
        std::string name_;
        std::deque<ParsingExpression> rules_;
    };

    namespace PEG
    {
        class RuleBase
        {
        public:
            RuleBase(ParsingExpressionGrammar& grammar, const std::string& name)
                : grammar_(grammar), name_(name)
            {
                rule_ = &(grammar_.add_rule(name));

                if (rule_->type != ParsingExpression::Undefined)
                {
                    throw std::runtime_error(std::string("Rule redefinition: ") + name);
                }
            }

            ParsingExpression* expr() const
            {
                return rule_;
            }

            ParsingExpression* operator* ()
            {
                auto& rule = grammar_.make_rule();
                rule.type = ParsingExpression::OneOrMore;
                rule.args.push_back(rule_);
                return &rule;
            }

        protected:
            void add_(ParsingExpression& rule)
            {
                if (rule_->type == ParsingExpression::OrderedChoice)
                {
                    auto& r = rule_->args.back();

                    if (r->type != ParsingExpression::Sequence)
                    {
                        auto& seq = grammar_.make_rule();
                        seq.type = ParsingExpression::Sequence;
                        seq.allow_spaces = rule_->allow_spaces;
                        seq.args.push_back(r);
                        rule_->args.back() = &seq;
                    }

                    rule_->args.back()->args.push_back(&rule);
                }
                else
                {
                    if (rule_->type != ParsingExpression::Sequence)
                    {
                        auto& tmp = grammar_.make_rule();
                        tmp.type = ParsingExpression::Sequence;
                        tmp.allow_spaces = rule_->allow_spaces;
                        tmp.swap(*rule_);
                        rule_->args.push_back(&tmp);
                    }

                    rule_->args.push_back(&rule);
                }
            }

            void add_(const char* str)
            {
                if (rule_->type == ParsingExpression::TerminalSequence)
                {
                    rule_->terminals += str;
                }
                else
                {
                    auto& rule = grammar_.make_rule();
                    rule.type = ParsingExpression::TerminalSequence;
                    rule.terminals = str;
                    add_(rule);
                }
            }

            ParsingExpression* current_rule()
            {
                return (rule_->type == ParsingExpression::OrderedChoice ? rule_->args.back() : rule_);
            }

            void new_terminal_sequence_(const char* str)
            {
                grammar_.add_rule(rule_->name);
                auto rule = current_rule();
                rule->type = ParsingExpression::TerminalSequence;
                rule->terminals = str;
            }

            void new_sequence_(const RuleBase& r)
            {
                if (rule_->type == ParsingExpression::OrderedChoice)
                {
                    rule_->args.push_back(r.expr());
                }
                else
                {
                    grammar_.add_rule(rule_->name);
                    auto rule = current_rule();
                    rule->type = ParsingExpression::Sequence;
                    rule->args.push_back(r.expr());
                }
            }

        protected:
            ParsingExpression* rule_;
            ParsingExpressionGrammar& grammar_;
            const std::string name_;
        };

        class CharacterClass : public RuleBase
        {
        public:
            CharacterClass(ParsingExpressionGrammar& grammar, const std::string& name)
                : RuleBase(grammar, name)
            {}

            CharacterClass& operator<< (const char* str)
            {
                create_(str);
                return *this;
            }

            CharacterClass& operator<< (const CharacterClass& cc)
            {
                create_(cc.rule_->terminals.c_str());
                return *this;
            }

            CharacterClass& operator| (const CharacterClass& cc)
            {
                rule_->terminals += cc.rule_->terminals;
                return *this;
            }

            CharacterClass& operator| (const char* str)
            {
                rule_->terminals += str;
                return *this;
            }

            CharacterClass& operator| (char ch)
            {
                rule_->terminals += ch;
                return *this;
            }

        private:
            void create_(const char* str)
            {
                if (!rule_->terminals.empty())
                {
                    throw std::runtime_error(std::string("Redefinition of CharacterClass '") + rule_->name + "'");
                }

                rule_->type = ParsingExpression::TerminalClass;
                rule_->terminals = str;
            }
        };

        class Token : public RuleBase
        {
        public:
            Token(ParsingExpressionGrammar& grammar, const std::string& name)
                : RuleBase(grammar, name)
            {}

            Token& operator<< (const char* str)
            {
                new_terminal_sequence_(str);
                return *this;
            }

            Token& operator<< (char ch)
            {
                const std::string str = { ch };
                new_terminal_sequence_(str.c_str());
                return *this;
            }

            Token& operator<< (const CharacterClass& cc)
            {
                new_sequence_(cc);
                return *this;
            }

            Token& operator<< (const Token& tok)
            {
                new_sequence_(tok);
                return *this;
            }

            Token& operator<< (ParsingExpression* rule)
            {
                if (rule->type == ParsingExpression::OneOrMore && rule->name.empty())
                {
                    grammar_.add_rule(rule_->name);
                    auto r = current_rule();
                    r->copy(*rule);
                }

                return *this;
            }

            Token& operator| (const Token& tok)
            {
                new_sequence_(tok);
                return *this;
            }

            Token& operator& (const CharacterClass& cc)
            {
                add_(*cc.expr());
                return *this;
            }

            Token& operator& (const Token& t)
            {
                add_(*t.expr());
                return *this;
            }

            Token& operator& (ParsingExpression* rule)
            {
                if (rule->type == ParsingExpression::OneOrMore && rule->name.empty())
                {
                    add_(*rule);
                }

                return *this;
            }

            Token& operator& (char ch)
            {
                const std::string str = { ch };
                add_(str.c_str());
                return *this;
            }

            Token& operator& (const char* str)
            {
                add_(str);
                return *this;
            }
        };

        class ProductionRule : public RuleBase
        {
        public:
            ProductionRule(ParsingExpressionGrammar& grammar, const std::string& name)
                : RuleBase(grammar, name)
            {
                rule_->allow_spaces = true;
            }

            ProductionRule& operator& (const RuleBase& r)
            {
                add_(*r.expr());
                return *this;
            }

            ProductionRule& operator<< (const Token& tok)
            {
                new_sequence_(tok);
                return *this;
            }

            ProductionRule& operator<< (const CharacterClass& cc)
            {
                new_sequence_(cc);
                return *this;
            }

            ProductionRule& operator<< (const ProductionRule& pr)
            {
                new_sequence_(pr);
                return *this;
            }

            ProductionRule& operator<< (char ch)
            {
                const std::string str = { ch };
                new_terminal_sequence_(str.c_str());
                return *this;
            }

            ProductionRule& operator<< (const char* str)
            {
                new_terminal_sequence_(str);
                return *this;
            }

            ProductionRule& operator& (char ch)
            {
                const std::string str = { ch };
                add_(str.c_str());
                return *this;
            }

            ProductionRule& operator& (const char* str)
            {
                add_(str);
                return *this;
            }

            ProductionRule& operator| (const RuleBase& pr)
            {
                if (rule_->type != ParsingExpression::OrderedChoice)
                {
                    grammar_.add_rule(rule_->name);
                    rule_->args.back() = pr.expr();
                }
                else
                {
                    rule_->args.push_back(pr.expr());
                }

                return *this;
            }

            ProductionRule& operator| (const char* str)
            {
                new_terminal_sequence_(str);
                return *this;
            }

            ProductionRule& operator| (ParsingExpression* rule)
            {
                if (rule->type == ParsingExpression::OneOrMore && rule->name.empty())
                {
                    grammar_.add_rule(rule_->name);
                    auto r = current_rule();
                    r->copy(*rule);
                }

                return *this;
            }

            ProductionRule& operator<< (ParsingExpression* rule)
            {
                if (rule->type == ParsingExpression::OneOrMore && rule->name.empty())
                {
                    grammar_.add_rule(rule_->name);
                    auto r = current_rule();
                    r->copy(*rule);
                    r->allow_spaces = true;
                }

                return *this;
            }
        };
    }
}
