#pragma once

#include "Grammar.h"
#include "Production.h"

namespace prime
{
    class ProductionBuilder
    {
    public:
        ProductionBuilder(Grammar& grammar, const std::string& name, bool allow_spaces)
        :
            grammar_(grammar),
            name_(name),
            allow_spaces_(allow_spaces)
        {}

        void new_production()
        {
            production_ = &grammar_.add_production(name_, allow_spaces_);
        }

        const std::string& getName() const
        {
            return name_;
        }

    protected:
        Grammar& grammar_;
        std::string name_;
        bool allow_spaces_;
        Production* production_ = nullptr;
    };
}
