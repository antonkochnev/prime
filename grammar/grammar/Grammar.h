#pragma once

#include <vector>

#include "Production.h"

namespace prime
{
    class Grammar
    {
    public:
        typedef std::vector<Production> Productions;

    public:
        Grammar(const std::string& name)
        :
            name_(name)
        {}

        const std::string& name() const
        {
            return name_;
        }

        const Productions& get_productions() const
        {
            return productions_;
        }

        Production& add_production(const std::string& name, bool allow_spaces)
        {
            const std::size_t priority = productions_.size();
            productions_.emplace_back(name, priority, allow_spaces);
            return productions_.back();
        }

        std::ostream& print(std::ostream& os) const
        {
            for (const Production& p : productions_)
            {
                os << p.name() << " -> ";

                for (const auto& e : p.elements())
                {
                    if (e.is_terminal())
                    {
                        os << e.as_terminal() << ' ';
                    }
                    else
                    {
                        os << e.as_nonterminal() << ' ';
                    }
                }

                os << std::endl;
            }

            return os;
        }

    protected:
        std::string name_;
        Productions productions_;
    };
}
