#include "tinytests/tinytests.h"

#include "grammar/ParsingExpression.h"
#include "grammar/ParsingExpressionGrammar.h"
#include "grammar/PackratParser.h"
#include "grammar/PEGParser.h"
#include "grammar/PEGEarleyParser.h"
#include "grammar/NonterminalUtils.h"

using namespace prime;
using namespace PEG;

std::string toString(const Nonterminal& arg);
std::string toString(const NonterminalPtr& arg);

TEST_SUITE_BEGIN(PEGParserTest)
{
    add("TestParsingExpressionDSL", &PEGParserTest::TestParsingExpressionDSL);
    add("TestParse1<PEGParser>", &PEGParserTest::TestParse1<PEGParser>);
    add("TestParse1<PackratParser>", &PEGParserTest::TestParse1<PackratParser>);
    add("TestParse1<PEGEarleyParser>", &PEGParserTest::TestParse1<PEGEarleyParser>);
    add("TestSuccessorChange<PEGParser>", &PEGParserTest::TestSuccessorChange<PEGParser>);
    add("TestSuccessorChange<PackratParser>", &PEGParserTest::TestSuccessorChange<PackratParser>);
    add("TestSuccessorChange<PEGEarleyParser>", &PEGParserTest::TestSuccessorChange<PEGEarleyParser>);
    add("TestParse2<PEGParser>", &PEGParserTest::TestParse2<PEGParser>);
    add("TestParse2<PackratParser>", &PEGParserTest::TestParse2<PackratParser>);
    add("TestParse2<PEGEarleyParser>", &PEGParserTest::TestParse2<PEGEarleyParser>);
    add("TestDoubleOneOrMore<PEGParser>", &PEGParserTest::TestDoubleOneOrMore<PEGParser>);
    add("TestDoubleOneOrMore<PackratParser>", &PEGParserTest::TestDoubleOneOrMore<PackratParser>);
    add("TestDoubleOneOrMore<PEGEarleyParser>", &PEGParserTest::TestDoubleOneOrMore<PEGEarleyParser>);
    add("TestParse1<PEGEarleyParser>", &PEGParserTest::TestParse1<PEGEarleyParser>);
}

void TestParsingExpressionDSL()
{
    ParsingExpressionGrammar grammar("TEST");
    CharacterClass a1(grammar, "a1"); a1 << "a";
    Token a2(grammar, "a2"); a2 << *a1;
    Token a3(grammar, "a3"); a3 << "aa";
    ProductionRule a4(grammar, "a4"); a4 << a2 | a3;
    ProductionRule a5(grammar, "a5"); a5 << a4;
    assertTrue(a4.expr()->type == ParsingExpression::OrderedChoice);

    ProductionRule a6(grammar, "a6");
    a6 << '~' & a5;
    assertTrue(a6.expr()->type == ParsingExpression::Sequence);
    assertTrue(a6.expr()->name == "a6");
}

template<typename ParserType>
void TestParse1()
{
    ParsingExpressionGrammar grammar("TEST");
    Token a1(grammar, "a1"); a1 << "include";
    CharacterClass a2(grammar, "a2"); a2 << "ABCD";
    Token a3(grammar, "a3"); a3 << *a2;
    ProductionRule a4(grammar, "a4"); a4 << a1 | a3;

    ParserType parser_a1(*a1.expr());
    parser_a1.set_spaces(" ");

    {
        const auto res = parser_a1.parse("include");
        assertNotNull(res);
        assertEquals("include", toString(res));
    }

    {
        const auto res = parser_a1.parse("includ");
        assertNull(res);
    }

    ParserType parser_a3(*a3.expr());
    parser_a3.set_spaces(" ");

    {
        const auto res = parser_a3.parse("AABBDD");
        assertNotNull(res);
        assertEquals("AABBDD", toString(res));
        assertEquals(1U, res->size());
        assertTrue((*res)[0].is_terminal());
    }

    ParserType parser_a4(*a4.expr());
    parser_a4.set_spaces(" ");

    {
        const auto res = parser_a4.parse("AABBDD");
        assertNotNull(res);
        assertEquals("AABBDD", toString(res));
    }

    {
        const auto res = parser_a4.parse("  AABBDD");
        assertNotNull(res);
        assertEquals("AABBDD", toString(res));
    }

    {
        const auto res = parser_a4.parse("AABBDD ");
        assertNotNull(res);
        assertEquals("AABBDD", toString(res));
    }

    {
        const auto res = parser_a4.parse("AA BBDD");
        assertNull(res);
    }

    ProductionRule a5(grammar, "a5"); a5 << *a2;
    ProductionRule a6(grammar, "a6"); a6 << a1 | a5;

    ParserType parser_a6(*a6.expr());
    parser_a6.set_spaces(" ");

    {
        const auto res = parser_a6.parse("AA   BBDD ");
        assertNotNull(res);
        assertEquals("AABBDD", toString(res));
    }

    ProductionRule a7(grammar, "a7"); a7 << '(' & a6 & ')';

    ParserType parser_a7(*a7.expr());
    parser_a7.set_spaces(" ");

    {
        const auto res = parser_a7.parse("( AA BBDD )");
        assertNotNull(res);
        assertEquals("(AABBDD)", toString(res));
    }

    ProductionRule a8(grammar, "a8"); a8 << '(' & a3 & ')';

    ParserType parser_a8(*a8.expr());
    parser_a8.set_spaces(" ");

    {
        const auto res = parser_a8.parse(" ( AA ) ");
        assertNotNull(res);
        assertEquals("(AA)", toString(res));
    }
}

template<typename ParserType>
void TestDoubleOneOrMore()
{
    ParsingExpressionGrammar grammar("TEST");
    CharacterClass cc(grammar, "cc"); cc << "a";
    Token A(grammar, "A"); A << *cc;
    ProductionRule B(grammar, "B"); B << *A;

    {
        ParserType parser(*B.expr());
        const auto res = parser.parse("aaaa");
        assertNotNull(res);
        assertEquals("aaaa", toString(res));
    }
}

template<typename ParserType>
void TestSuccessorChange()
{
    ParsingExpressionGrammar grammar("TEST");
    CharacterClass a1(grammar, "a1"); a1 << "a";
    ProductionRule a2(grammar, "a2"); a2 << *a1;
    ProductionRule a3(grammar, "a3"); a3 << "aa";
    ProductionRule a4(grammar, "a4"); a4 << a2 | a3;
    ProductionRule a5(grammar, "a5"); a5 << a4;

    {
        ParserType parser(*a4.expr());
        parser.set_spaces(" ");
        const auto res = parser.parse("aa");
        assertNotNull(res);
        assertEquals("aa", toString(res));
        assertTrue(!(*res)[0].is_terminal());
        assertEquals("a2", (*res)[0].nonterminal()->name())
    }

    {
        ParserType parser(*a5.expr());
        parser.set_spaces(" ");
        const auto res = parser.parse("aa");
        assertNotNull(res);
        assertEquals("aa", toString(res));
    }
}

template<typename ParserType>
void TestParse2()
{
    ParsingExpressionGrammar grammar("TEST");
    CharacterClass digit(grammar, "digit");
    digit << "0123456789";
    ProductionRule expr(grammar, "expr");
    Token number(grammar, "number");
    number << *digit;
    ProductionRule mult(grammar, "mult");
    mult << expr & '*' & expr;
    ProductionRule add(grammar, "add");
    add << expr & '+' & expr;
    expr << mult | add | number;

    ParserType parser_expr(*expr.expr());
    parser_expr.set_spaces(" ");

    {
        const auto res = parser_expr.parse("1+2");
        assertNotNull(res);
        assertEquals("1+2", toString(res));
    }
}

TEST_SUITE_END
