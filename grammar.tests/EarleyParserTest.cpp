#include "tinytests/tinytests.h"

#include "grammar/CharacterClass.h"
#include "grammar/EarleyParser.h"
#include "grammar/Grammar.h"
#include "grammar/NonterminalUtils.h"
#include "grammar/ProductionRule.h"
#include "grammar/Token.h"

using namespace prime;

std::string toString(const Nonterminal& arg)
{
    return print(arg);
}

std::string toString(const NonterminalPtr& arg)
{
    return toString(*arg);
}

TEST_SUITE_BEGIN(EarleyParserTest)
    {
        add("TestCharacterClass", &EarleyParserTest::TestCharacterClass);
        add("TestToken", &EarleyParserTest::TestToken);
        add("TestProductionRule", &EarleyParserTest::TestProductionRule);
        add("TestExpression", &EarleyParserTest::TestExpression);
    }

    void TestCharacterClass()
    {
        Grammar grammar("TEST");
        CharacterClass letter(grammar, "letter");
        letter << "ABCDEFGH";

        EarleyParser parser(grammar, letter.getName());
        parser.set_spaces(" ");

        {
            const auto res = parser.parse("A");
            assertNotNull(res);
        }

        {
            const auto res = parser.parse("Z");
            assertNull(res);
        }
    }

    void TestToken()
    {
        Grammar grammar("TEST");
        CharacterClass letter(grammar, "letter");
        letter << "ABCDEFGH";

        CharacterClass digit(grammar, "digit");
        digit << "1234567890";

        Token word(grammar, "word");
        word << letter;
        word << word & letter;
        word << word & digit;

        Token tok1(grammar, "tok1");
        tok1 << 'a' & 'b' & 'c' & 'd';

        ProductionRule wordPair(grammar, "wordPair");
        wordPair << word & '|' & word;
        wordPair << tok1 & '|' & tok1;

        EarleyParser parser(grammar, word.getName());
        parser.set_spaces(" ");

        {
            const auto res = parser.parse("A");
            assertNotNull(res);
            assertEquals("A", toString(res));
        }

        {
            const auto res = parser.parse("1");
            assertNull(res);
        }

        {
            const auto res = parser.parse("AB");
            assertNotNull(res);
            assertEquals("AB", toString(res));
        }

        {
            const auto res = parser.parse("A2C");
            assertNotNull(res);
            assertEquals("A2C", toString(res));
        }

        {
            EarleyParser parser2(grammar, wordPair.getName());
            parser2.set_spaces(" ");
            const auto res = parser2.parse("A2C   |    A1C");
            assertNotNull(res);
            assertEquals("A2C|A1C", toString(res));
        }

        {
            EarleyParser parser2(grammar, wordPair.getName());
            parser2.set_spaces(" ");
            const auto res = parser2.parse("abcd   |    abcd");
            assertNotNull(res);
            assertEquals("abcd|abcd", toString(res));
        }
    }

    void TestExpression()
    {
        Grammar grammar("TEST");
        CharacterClass digit(grammar, "digit");
        digit << "0123456789";
        ProductionRule expr(grammar, "expr");
        Token number(grammar, "number");
        number << digit;
        ProductionRule mult(grammar, "mult");
        mult << expr & '*' & expr;
        ProductionRule add(grammar, "add");
        add << expr & '+' & expr;
        expr << mult | add | number;

        EarleyParser parser_expr(grammar, expr.getName());
        parser_expr.set_spaces(" ");

        {
            const auto res = parser_expr.parse("1+2");
            assertNotNull(res);
            assertEquals("1+2", toString(res));
        }
    }

    void TestProductionRule()
    {
        Grammar grammar("TEST");
        CharacterClass lower_alpha(grammar, "lower_alpha");
        lower_alpha << "abcdefghijklmnopqrstuvwxyz";

        CharacterClass upper_alpha(grammar, "upper_alpha");
        upper_alpha << "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        CharacterClass numeric(grammar, "numeric");
        numeric << "0123456789";

        CharacterClass printable_char(grammar, "printable_char");
        printable_char << lower_alpha | upper_alpha | numeric;

        CharacterClass single_quote(grammar, "single_quote");
        single_quote << "'";

        CharacterClass sq_char(grammar, "sq_char");
        sq_char << "()*+,-./~" | lower_alpha | upper_alpha | numeric;

        Token sq_char_seq(grammar, "sq_char_seq");
        sq_char_seq << sq_char;
        sq_char_seq << sq_char_seq & sq_char;

        ProductionRule single_quoted(grammar, "single_quoted");
        single_quoted << single_quote & sq_char_seq & single_quote;

        ProductionRule file_name(grammar, "file_name");
        file_name << single_quoted;

        ProductionRule Include(grammar, "Include");
        Include << "include(" & file_name & ").";

        Token word(grammar, "word");
        word << upper_alpha;
        word << word & upper_alpha;
        word << word & numeric;

        ProductionRule variable(grammar, "variable");
        variable << word;

        ProductionRule formula(grammar, "formula");
        formula << variable;
        formula << '(' & formula & ')';

        EarleyParser parser(grammar, formula.getName());
        parser.set_spaces(" ");

        {
            const auto res = parser.parse("A");
            assertNotNull(res);
            assertEquals("A", toString(res));
        }

        {
            const auto res = parser.parse("(A)");
            assertNotNull(res);
            assertEquals("(A)", toString(res));
        }

        {
            const auto res = parser.parse("( A)");
            assertNotNull(res);
            assertEquals("(A)", toString(res));
        }

        {
            const auto res = parser.parse("( A )");
            assertNotNull(res);
            assertEquals("(A)", toString(res));
        }

        {
            const auto res = parser.parse("(  ( AB )   ) ");
            assertNotNull(res);
            assertEquals("((AB))", toString(res));
        }

        {
            const auto res = parser.parse("(  ( A B )   ) ");
            assertNull(res);
        }

        EarleyParser parser2(grammar, Include.getName());
        parser2.set_spaces(" ");

        {
            const auto res = parser2.parse("include('Axioms/SET001-0.ax').");
            assertNotNull(res);
            assertEquals("include('Axioms/SET001-0.ax').", toString(res));
        }
    }
TEST_SUITE_END
