#include <iostream>

#include "tinytests/tinytests.h"

int main(int argc, char* argv[])
{
    tinytests::TestRunner::instance().run(std::cout, argc, argv);
    return 0;
}
