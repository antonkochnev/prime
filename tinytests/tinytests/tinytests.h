#pragma once

#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <typeinfo>
#include <vector>

#define TEST_SUITE_BEGIN(Name) namespace { struct Name : public tinytests::TestSuite<Name> { Name() : tinytests::TestSuite<Name>(#Name)
#define TEST_SUITE_END } testSuite; }
#define assertNotNull(val) assertNotNull_(val, __FILE__, __LINE__);
#define assertNull(val) assertNull_(val, __FILE__, __LINE__);
#define assertEquals(expected, actual) assertEquals_(expected, actual, __FILE__, __LINE__);
#define assertTrue(cond) if (!(cond)) { std::ostringstream oss; oss << "Failed: '" << #cond << "' " << __FILE__ << ':' << __LINE__; throw std::runtime_error(oss.str()); }

template<typename T>
inline std::string toString(const T& arg)
{
    std::ostringstream oss;
    oss << "0x" << static_cast<const void*>(&arg);
    return oss.str();
}

template<>
inline std::string toString<std::string>(const std::string& arg)
{
    return arg;
}

inline std::string toString(const char* arg)
{
    return arg;
}

template<>
inline std::string toString<int>(const int& arg)
{
    return std::to_string(arg);
}

template<>
inline std::string toString<std::size_t>(const std::size_t& arg)
{
    return std::to_string(arg);
}

namespace tinytests
{
    class TestsReport
    {
    public:
        enum class TestStatus
        {
            NOT_RUN,
            OK,
            FAILED,
            SKIPPPED
        };

        struct TestReport
        {
            std::string name;
            TestStatus status = TestStatus::NOT_RUN;
            std::string message;
        };

    public:
        std::vector<TestReport> entries;
        std::size_t count = 0;
        std::size_t failed = 0;
        std::size_t skipped = 0;

    public:
        void print(std::ostream& oss)
        {
            if (failed == 0 && entries.empty())
            {
                oss << "OK (" << count << ")";

                if (skipped)
                {
                    oss << " skipped: " << skipped;
                }
            }
            else
            {
                oss << "passed: " << (count - failed - skipped) << " failed: " << failed;

                if (skipped)
                {
                    oss << " skipped: " << skipped;
                }

                oss << std::endl;

                for (const auto& entry : entries)
                {
                    if (entry.status == TestStatus::FAILED)
                    {
                        oss << entry.name << " (" << toString(entry.status) << ") " << entry.message << std::endl;
                    }
                }
            }
        }

        static const char* toString(TestStatus status)
        {
            if (status == TestStatus::NOT_RUN)
            {
                return "NOT_RUN";
            }
            else if (status == TestStatus::OK)
            {
                return "OK";
            }
            else if (status == TestStatus::FAILED)
            {
                return "FAILED";
            }
            else if (status == TestStatus::SKIPPPED)
            {
                return "SKIPPPED";
            }

            return "";
        }
    };

    class TestBase
    {
    public:
        virtual ~TestBase()
        {}

        virtual void run(TestsReport& reports) = 0;
    };

    typedef std::shared_ptr<TestBase> TestBasePtr;

    class TestRunner
    {
    public:
        void registerTest(const std::string& name, TestBase* test)
        {
            tests_.emplace(name, test);
        }

        void run(std::ostream& oss, int argc, char* argv[])
        {
            TestsReport reports;
            std::set<std::string> filter;

            if (argc > 1)
            {
                filter.insert(argv[1]);
            }

            for (const auto& test : tests_)
            {
                if (filter.empty() || (filter.find(test.first) != filter.end()))
                {
                    test.second->run(reports);
                }
            }

            reports.print(oss);
        }

        static TestRunner& instance()
        {
            static TestRunner instance_;
            return instance_;
        }

    private:
        std::map<std::string, TestBase*> tests_;

    private:
        TestRunner() = default;
        TestRunner(const TestRunner&) = default;
        TestRunner(TestRunner&&) = default;
    };

    class TestCase : public TestBase
    {
    public:
        TestCase(std::function<void()> func)
            : func_(func)
        {}

        virtual ~TestCase()
        {}

        virtual void run(TestsReport&) override
        {
            func_();
        }

    private:
        std::function<void()> func_;
    };

    template<typename Impl>
    class TestSuite : public TestBase
    {
    public:
        typedef std::function<void()> RunUnit;
        typedef void (Impl::*ImplFunc)();
        typedef std::map<std::string, TestBasePtr> TestCases;

    public:
        TestSuite(const char* n)
            : name_(n)
        {
            TestRunner::instance().registerTest(name_, this);
        }

        virtual ~TestSuite()
        {}

        virtual void run(TestsReport& reports) override
        {
            bool skipAll = false;
            reports.count += testCases_.size();

            if (beforeClass_)
            {
                if (run_(beforeClass_, "BeforeClass", reports) != TestsReport::TestStatus::OK)
                {
                    skipAll = true;
                    printReport_(reports.entries.back());
                }
            }

            for (const auto& testCase : testCases_)
            {
                if (skipAll)
                {
                    TestsReport::TestReport report;
                    report.status = TestsReport::TestStatus::SKIPPPED;
                    report.name = fullName_(testCase.first);
                    reports.entries.emplace_back(report);
                    ++reports.skipped;
                }
                else
                {
                    bool skipTest = false;

                    if (setUp_)
                    {
                        std::ostringstream oss;
                        oss << testCase.first << ".SetUp";

                        if (run_(setUp_, oss.str(), reports) != TestsReport::TestStatus::OK)
                        {
                            skipTest = true;
                            printReport_(reports.entries.back());
                        }
                    }

                    if (skipTest)
                    {
                        TestsReport::TestReport report;
                        report.status = TestsReport::TestStatus::SKIPPPED;
                        report.name = fullName_(testCase.first);
                        reports.entries.emplace_back(report);
                        ++reports.skipped;
                        printReport_(report);
                    }
                    else
                    {
                        if (run_(std::bind(&TestBase::run, std::ref(*testCase.second), std::ref(reports)), testCase.first, reports) == TestsReport::TestStatus::FAILED)
                        {
                            ++reports.failed;
                        }

                        printReport_(reports.entries.back());
                    }

                    if (!skipTest && tearDown_)
                    {
                        std::ostringstream oss;
                        oss << testCase.first << ".TearDown";
                        
                        if (run_(tearDown_, oss.str(), reports) != TestsReport::TestStatus::OK)
                        {
                            printReport_(reports.entries.back());
                        }
                    }
                }
            }

            if (!skipAll && afterClass_)
            {
                const auto status = run_(afterClass_, "AfterClass", reports);
            }
        }

    protected:
        void add(const char* name, ImplFunc implFunc)
        {
            testCases_.emplace(name, std::make_shared<TestCase>(std::bind(implFunc, static_cast<Impl*>(this))));
        }

        void beforeClass(ImplFunc implFunc)
        {
            beforeClass_ = std::bind(implFunc, this);
        }

        void afterClass(ImplFunc implFunc)
        {
            afterClass_ = std::bind(implFunc, this);
        }

        void setUp(ImplFunc implFunc)
        {
            setUp_ = std::bind(implFunc, this);
        }

        void tearDown(ImplFunc implFunc)
        {
            tearDown_ = std::bind(implFunc, this);
        }

        template<typename T>
        void assertNotNull_(const T& ptr, const char* file, int line)
        {
            if (ptr == nullptr)
            {
                std::ostringstream oss;
                oss << "Value is null. " << file << ':' << line;
                throw std::runtime_error(oss.str());
            }
        }

        template<typename T>
        void assertNull_(const T& ptr, const char* file, int line)
        {
            if (ptr != nullptr)
            {
                std::ostringstream oss;
                oss << "Expected: null, Actual: " << toString(*ptr) << ' ' << file << ':' << line;
                throw std::runtime_error(oss.str());
            }
        }

        template<typename T1, typename T2>
        void assertEquals_(const T1& expected, const T2& actual, const char* file, int line)
        {
            if (!(expected == actual))
            {
                std::ostringstream oss;
                oss << "Expected: " << toString(expected) << ", Actual: " << toString(actual) << ' ' << file << ':' << line;
                throw std::runtime_error(oss.str());
            }
        }

    private:
        std::string name_;
        RunUnit beforeClass_;
        RunUnit afterClass_;
        RunUnit setUp_;
        RunUnit tearDown_;
        TestCases testCases_;

    private:
        TestsReport::TestStatus run_(RunUnit func, const std::string& name, TestsReport& reports)
        {
            TestsReport::TestReport report;
            report.name = fullName_(name);

            try
            {
                func();
                report.status = TestsReport::TestStatus::OK;
            }
            catch (const std::exception& e)
            {
                report.status = TestsReport::TestStatus::FAILED;
                report.message = e.what();
            }
            catch (...)
            {
                report.status = TestsReport::TestStatus::FAILED;
                report.message = "Unknown error";
            }

            reports.entries.emplace_back(report);
            return report.status;
        }

        std::string fullName_(const std::string& unitName) const
        {
            std::ostringstream oss;
            oss << name_ << "." << unitName;
            return oss.str();
        }

        void printReport_(const TestsReport::TestReport& report)
        {
            std::cout << report.name << " (" << TestsReport::toString(report.status) << ") " << report.message << std::endl;
        }
    };
}
